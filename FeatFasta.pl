#!/usr/bin/perl

#-----------------------------------------------#
#  Author:  Sylvain Marthey and Damien Barreau  #
#  Project: Tilseg                              #
#  Release: 1.0                                 #
#  Date :   19/05/2010                          #
#-----------------------------------------------#

=head1 NAME

 FeatFasta.pl

=head1 SYNOPSIS

 FeatFasta.pl --fasta_file_in <file path> --gff_file <file path> --fasta_file_out <file path>

=head1 OPTIONS

	--fasta_file_in		file containing sequence fasta format

	--gff_file		GFF file containing the segment info

	--fasta_file_out	file which will contains the results

=over

=back

=head1 B<DESCRIPTION>

 Ce srcipt va traiter le fichier GFF passé en argumant.
 Genere un fichier resultat au format fasta, contenant les sequences en fonction des features passes en arguments dans le fichier GFF.
=cut

use strict;
use Getopt::Long;
use Pod::Usage;
use Data::Dumper;
use Bio::SeqIO;
use Bio::Seq;
use Bio::Perl;

my $fasta_file_in;
my $gff_file;
my $fasta_file_out;
my $help;

GetOptions(	"help|?" => \$help,
		"fasta_file_in:s" => \$fasta_file_in,
		"gff_file:s" => \$gff_file,
		"fasta_file_out:s" => \$fasta_file_out,
	)
  or
  pod2usage(-message=>"Try `$0' for more information.", -exitval => 2,-verbose=> 0);

if (($fasta_file_in eq "") || !(-f($fasta_file_in)))
  {
     pod2usage(-message=>"--fasta_file_in parameter missing or is not valid.\nTry `$0 --help' for more information.",
	      -exitval => 2,-verbose=> 0);
  }

if (($gff_file eq "") || !(-f($gff_file)))
  {
     pod2usage(-message=>"--gff_file parameter missing or is not valid.\nTry `$0 --help' for more information.",
	      -exitval => 2,-verbose=> 0);
  }

if (($fasta_file_out eq "" ) || !(-f($fasta_file_out)))
  {
     pod2usage(-message=>"--fasta_file_out parameter missing or is not valid.\nTry `$0 --help' for more information.",
	      -exitval => 2,-verbose=> 0);
  } 

extractSubSeqsFromFastaAndGFF(		fasta_file_in => $fasta_file_in,
					gff_file => $gff_file,
					fasta_file_out => $fasta_file_out);

=begin function

Function : extractSubSeqsFromFastaAndGFF
Description :	fonction qui permet de recuperer les sequences dans le fichier d'entree fasta correspondant à chaques feature decrits dans le fichier GFF.
Usage : extractSubSeqsFromFastaAndGFF(		fasta_file_in => String : path du fichier fasta,
						gff_file => String : path du fichier GFF,
						fasta_file_out => String : path du fichier de sorti contenant les resultats);
Parameters : 	see Usage
Returns :	none
Version : v1.0

=end function

=cut

sub extractSubSeqsFromFastaAndGFF{
	my %args = @_;
	my $fasta_file_in = $args{fasta_file_in};
	my $gff_file = $args{gff_file};
	my $fasta_file_out = $args{fasta_file_out};

	my $seq_obj;
	my $seq_by_feat;
	my @feature_by_seq;
	my @seq_objs;
	my %seqs_list;

	# on recupere les infos sur les features donnes dans le fichier GFF.
	my %features_Info = %{getFeaturesFromGFF(	gff_file => $gff_file)};

	# utilisation de fonction Bioperl permettant lire un fichier contenant des sequences.
	my $seqio_obj = Bio::SeqIO->new(-file => "$fasta_file_in", -format => "fasta");

	# pour chaque sequence dans le fichier fasta
	while($seq_obj = $seqio_obj->next_seq){
		my $id = ($seq_obj->primary_id);
		# si on ne doit pas recuperer des features sur cette sequence -> on passe
		next if (!$features_Info{$id});
		# sinon c'est qu'on doit extraire des features
		my @features = @{$features_Info{$id}};
		# on recupere la sequence.
		my $seq = $seq_obj->seq;
		# on reverse la sequence pour les features reverse.
		my $seq_rev = reverse_complement_as_string($seq_obj);
		# on ajoute la seq à la liste des seqs traitees:
		$seqs_list{$id} = 1;
		# pour chaque feature	
		for(my $j ; $j < scalar(@features); $j++){
				# on test si on est en rev ou fwd.
				if($features[$j]{strand} eq '+' || uc($features[$j]{strand}) =~ m/FWD/ || uc($features[$j]{strand}) =~ m/FORWARD/){
					# on selectionne la sequence correspondante aux features.
					$seq_by_feat = substr($seq,($features[$j]{start}-1),($features[$j]{stop}-$features[$j]{start}));
					# fonction Bioperl permettant de rentrer les informations sur une sequence.
					$seq_obj = Bio::Seq->new(	-seq => $seq_by_feat,
									-display_id => $features[$j]{segID});
				}else{
					# on selectionne la sequence correspondante aux features.
					$seq_by_feat = substr($seq_rev,($features[$j]{start}-1),($features[$j]{stop}-$features[$j]{start}));
					# fonction Bioperl permettant de rentrer les informations sur une sequence.
					$seq_obj = Bio::Seq->new(	-seq => $seq_by_feat,
									-display_id => $features[$j]{segID});
				}
		push(@seq_objs,$seq_obj);
		}
	}
	# on test si toutes les fetures du GFF ont pu etre extraites
	foreach my $k (keys(%features_Info)){
		if(!$seqs_list{$k}){
			print "Warning !! seq $k appears in the GFF file but not in the FASTA file\n";
		}
	}

	printSeqToFastaFile(	file_out => $fasta_file_out,
				seq_list => \@seq_objs);
}

=begin function

Function : getFeaturesFromGFF
Description : 	fonction qui permet de recuperer les infos sur les features contenu dans le fichier GFF passe en argument.
Usage : %{getFeaturesFromGFF(	gff_file => String : Path du fichier GFF contenant les infos)}
Parameters : see Usage
Returns : $VAR1 = {
			'seq' => [
					{
						'stop' => Int,
						'strand' => String,
						'segID' => String,
						'start' => Int 
					},
				]
		}
Version : v1.0

=end function

=cut

sub getFeaturesFromGFF{
	my %args = @_;
	my $gff_file = $args{gff_file};

	my %features_Info;
	open(IN,$gff_file) or die "sub getFeaturesFromGFF: Unable to open the file $gff_file .\n";

	<IN>;
	while(<IN>){
		chomp();
		my @line = split(/\t/);
		my $seq = $line[0];
		if(!($line[3] =~ m/\d+/) && ($line[3] ne '')){
			die "sub getFeaturesFromGFF: start parameter is missing or is not in the standard format (Int), please check it.\n";
		}
		if(!($line[4] =~ m/\d+/) && ($line[4] ne '')){
			die "sub getFeaturesFromGFF: stop parameter is missing or is not in the standard format (Int), please check it.\n";
		}
		if($line[8] eq ''){
			die "sub getFeaturesFromGFF: segID parameter is missing (String), please check it.\n";
		}
		if($line[6] ne '+' && $line[6] ne '-'){
			die "sub getFeaturesFromGFF: strand parameter is missing or is not in the standard format (+ or -), please check it.\n";
		}
		if($line[0] eq ''){
			die "sub getFeaturesFromGFF: seq parameter is missing (String), please check it.\n";
		}
		# on rentre les informations sur les features.
		my %feat = (	start => $line[3],
				stop => $line[4],
				strand => $line[6],
				segID => $line[8]);
		# si un feature existe deja pour cette seq, alors on ajoute celle-ci au tableau
		if(ref($features_Info{$seq}) eq 'ARRAY'){
			# on ajoute les infos d'un feature au tableau.
			push(@{$features_Info{$seq}},\%feat);	
		# sinon on creer le tableau de features correspondant à cette seq
		}else{
			my @temp= (\%feat) ;
			$features_Info{$seq} = \@temp;
		}
	}
	close IN;
	return \%features_Info;
}

=begin function

Function : printSeqToFastaFile
Description : fonction qui permet de printer dans un fichier au format fasta des sequences passees en parametre.
Usage : printSeqToFastaFile(	file_out => String : Pth du fichier de sortie au format fasta,
				seq_list => );
Parameters : seq_list => $VAR1 = [
					bless( {
						'primary_seq' => bless( {
										'display_id' => ,
										'desc' => ,
										'_seq_length' => ,
										'_root_verbose' => ,
										'seq' => ,
										'_nowarnonempty' => ,
										'alphabet' => 
									}, 'Bio::PrimarySeq' ),
						'_root_verbose' => 0
						}, 'Bio::Seq' ),
				]
Returns : none
Version : v1.0

=end function

=cut

sub printSeqToFastaFile{
	my %args = @_;
	my @seq_list = @{$args{seq_list}};
	my $file_out = $args{file_out};

	# fonction Bioperl qui permet d'ouvrir un fichier en ecriture.
	my $seqio_obj = Bio::SeqIO->new(-file => '>'.$file_out, -format => 'fasta');
	# on ecrit les sequences dans le fichier de sortie.
	$seqio_obj->write_seq(@seq_list);
}


