#!/usr/bin/perl

#------------------------------------------------#
#  Author:  Sylvain Marthey and Damien Barreau   #
#  Project: Tilseg                               #
#  Release: 2.0                                  #
#  Date :   17/12/2011                           #
#------------------------------------------------#

=head1 NAME

 TilSeg.pl

=head1 SYNOPSIS

 TilSeg.pl --files_dir <file path> --out_dir <dir path> [--probe_types 'type1,type2,..,typeN' --temp_dir <dir path> --FDRthresh <Integer or String> --group_size <Integer> --probes_length <Integer> --max_seg_length <Integer> --group_strand <String> --group_seqs <String> --Kmax <int> --merge_by_position <yes|no> --second_files_dir <file path> --start_from_gff <file_path>]

=head1 OPTIONS

	--files_dir		folder containing the pair files. Even if you use the start_from_gff option, you must give a folder path containg a .pair file (use as template by Tilseg)

	--out_dir		folder which will contains the results files

	--temp_dir		folder where the TilSeg software can create temporary files

	--probes_length		parameter which defined the lenght for each probes (default '50')

	--FDRthresh		FDRthresh (default '1e-3')

	--group_size		parameter which defined the group size for the segmentation (default '40000')

	--Kmax		parameter which defined the maximun number of segments inside a group_size (default 'group_size/10')

	--max_seg_length	parameter which defined the maximum length for each segment during the segmentation (default '1000')

	--group_strand		parameter which group the probe by their strand or not (default 'yes')

	--group_seqs		parameter which group the probe by their sequence or not (default 'yes')

	--probe_types		parameter which contain the probes categories which will be threated by TilSeg (default FORMARD and REVERSE)
				put your custom categories like this: 'cat1,cat2,cat3,...,catN'

	--merge_by_position	boolean parameter. If true the probes values will be merge according to their chromosome location. The value of all the probes sharing the same location (Forward/Reverse/Replicate) are meaning and considers as one.

	--second_files_dir	folder containing the pair files corresponding tio the second condition 
				=> in this case the the analysis will be comprataive between the two conditions, the log2(cond1/cond2) will be use to make the segmentation

	--start_from_gff	file containing values. Avoid the normalisation step and sample meaning step.


=over

=back

=head1 B<DESCRIPTION>

 Ce srcipt va traiter tous les fichiers pair contenus dans le dossier passé en argumant.
 Pour chaque fichier les valeurs seront normalisées (Normalization: log2 transformation and centered by slide).
 Ensuite une moyenne de ces valeurs normalisées sera également calculée.
 Trois fichiers seront générés :	- un fichier tabulé contenant les X valeurs normalisees et la moyenne
					- un fichier pair content les moyennes
					- un fichier gff contenant les moyennes
					- un fichier bed contenant les moyennes

=cut

use strict;
use Getopt::Long;
use Pod::Usage;
use Data::Dumper;
use runR;

my $in_dir;
my $out_dir;
my $temp_dir;
my $help;
my $FDRthresh;
my $group_size;
my $probes_length;
my $max_seg_length;
my $group_strand;
my $group_seqs;
my $probe_types;
my $Kmax;
my $merge_by_position;
my $in_dir_2;
my $use_strand;
my $start_from_gff;

GetOptions(	"help|?" => \$help,
		"files_dir:s" => \$in_dir,
		"start_from_gff:s" => \$start_from_gff,
		"out_dir:s" => \$out_dir,
		"temp_dir:s" => \$temp_dir,
		"FDRthresh:s" => \$FDRthresh,
		"group_size:s" => \$group_size,
		"probes_length:s" => \$probes_length,
		"max_seg_length:s" => \$max_seg_length,
		"group_strand:s" => \$group_strand,
		"group_seqs:s" => \$group_seqs,
		"probe_types:s" => \$probe_types,
		"Kmax:s" => \$Kmax,
		"merge_by_position:s" => \$merge_by_position,
		"second_files_dir:s" => \$in_dir_2
	   )
  or
  pod2usage(-message=>"Try `$0' for more information.", -exitval => 2,-verbose=> 0);

pod2usage(-exitval =>1, -verbose => 2) if ($help);

if (($in_dir eq "") || !(-d($in_dir)))
  {
     pod2usage(-message=>"--files_dir parameter missing or is not  a valid directory path.\nTry `$0 --help' for more information.",
	      -exitval => 2,-verbose=> 0);
  }
if ((($in_dir_2 ne "") && !(-d($in_dir))) && !$start_from_gff)
  {
     pod2usage(-message=>"--files_dir_2 parameter is not a valid directory path.\nTry `$0 --help' for more information.",
	      -exitval => 2,-verbose=> 0);
  }
if (($out_dir eq "" )|| !(-d($out_dir)))
  {
     pod2usage(-message=>"--out_dir parameter missing or is not valid.\nTry `$0 --help' for more information.",
	      -exitval => 2,-verbose=> 0);
  }  
if ($start_from_gff && !$start_from_gff)
  {
     pod2usage(-message=>"--start_from_gff parameter missing or is not file path.\nTry `$0 --help' for more information.",
	      -exitval => 2,-verbose=> 0);
  } 

if(!$FDRthresh){
	$FDRthresh = '1e-3';
}elsif(!($FDRthresh =~ m/0\.d+/) && !($FDRthresh =~ m/[1-9]e\-[1-9]+/)){
     pod2usage(-message=>"--FDRthresh parameter is not in the standard format.\nTry `$0 --help' for more information.",
	      -exitval => 2,-verbose=> 0);
}

if(!$group_size){
	$group_size = '40000';
}elsif(!($group_size =~ m/[1-9][0-9]*/)){
     pod2usage(-message=>"--group_size parameter is not in the standard format.\nTry `$0 --help' for more information.",
	      -exitval => 2,-verbose=> 0);
}

if(!$probes_length){
	$probes_length = '50';
}elsif(!($probes_length =~ m/[1-9][0-9]*/)){
     pod2usage(-message=>"--probes_length parameter is not in the standard format.\nTry `$0 --help' for more information.",
	      -exitval => 2,-verbose=> 0);
}

if(!$max_seg_length){
	$max_seg_length = '1000';
}elsif(!($max_seg_length =~ m/[1-9][0-9]*/)){
     pod2usage(-message=>"--max_seg_length parameter is not in the standard format.\nTry `$0 --help' for more information.",
	      -exitval => 2,-verbose=> 0);
}

if(!$group_strand){
	$group_strand = 'yes';
}elsif((!(uc($group_strand) =~ m/YES/)) && (!(uc($group_strand) =~ m/NO/))){
     pod2usage(-message=>"--group_strand parameter is not in the standard format.\nTry `$0 --help' for more information.",
	      -exitval => 2,-verbose=> 0);
}

if(!$group_seqs){
	$group_seqs = 'yes';
}elsif((!(uc($group_seqs) =~ m/YES/)) && (!(uc($group_seqs) =~ m/NO/))){
     pod2usage(-message=>"--group_seqs parameter is not in the standard format.\nTry `$0 --help' for more information.",
	      -exitval => 2,-verbose=> 0);
}

if(!$temp_dir){
	$temp_dir = $out_dir;
}

my @files ;
foreach my $f (@{getFileList($in_dir)}){
	
	if($f =~ m/.pair/){
		push(@files,$f);
	}
}


if(scalar(@files) == 0){
	die "$in_dir contains no PAIR file, check that yours files have the file extension .pair\n";
}

my $id_type;

# we define the id_type according to TilSeg parameters
# if id_type = 'id' the probes will be sort by probe_id -> the probe_id must be unique in the pair file
#                                                             -> the FWD and REV probes will considers differentes (expression)
# if id_type = 'loc' the probe will be sort by chromosome location -> all the probe sharing the same location will be merge (whitout strand distingtion)
#                                                                       -> the FWD an REV probes are merge
if(uc($merge_by_position) eq 'YES' || uc($merge_by_position)eq 'Y'){
	$id_type = 'loc';
}else{
	$id_type = 'id';
	$use_strand=1;
}

#####################################
# parse config file #################
#####################################
my $conf = Conf::getFileConfig('default');
if($probe_types){
	my %goods_cats;
	foreach my $cat (split(/,/,$probe_types)){
		$goods_cats{$cat} = 1;
	}
	$$conf{good_cats} = \%goods_cats;
}
# define the outputs
my $gff_file;
if(!$start_from_gff){
	$gff_file = $out_dir."/norm_mean_values.gff";
}else{
	$gff_file = $start_from_gff;
}
my $template_file = $in_dir."/".@files[0];

#####################################################################
##### process the data normalisation, mean calculation ##############
#####################################################################
my $datas;
if(!$start_from_gff){
	# case of comparative analysis
	if($in_dir_2){

		# extract datats from the first set of files
		print "Process the first group of files : $in_dir\n";
		my $means1 = extractNormMeanValuesFromPair (	files => \@files,
			                                        conf => $conf,
			                                        id_type => $id_type,
								use_strand => $use_strand,
			                                        in_dir => $in_dir,
								normalized_file => $out_dir."/norm_mean_values_1.csv");

		my @files_2 = @{getFileList($in_dir_2)};
		print "Process the second group of files : $in_dir_2\n";
		my $means2 = extractNormMeanValuesFromPair (	files => \@files_2,
			                                        conf => $conf,
			                                        id_type => $id_type,
								use_strand => $use_strand,
			                                        in_dir => $in_dir_2,
								normalized_file => $out_dir."/norm_mean_values_2.csv");
		
		print "make the calcul of ratio for each probe\n";
		foreach my $k (keys(%{$means1})){
			$$datas{$k} = $$means1{$k}-$$means2{$k};
		}
	}else{
		$datas = extractNormMeanValuesFromPair (	files => \@files,
			                                        conf => $conf,
			                                        id_type => $id_type,
								use_strand => $use_strand,
			                                        in_dir => $in_dir,);
		
	}

	################################################
	print "generate the gff file\n"; ###############
	################################################

	# read the .pair template file, add values and generate the gff
	convertPairToGFF(	pair_file => $template_file,
				gff_file => $gff_file,
				id_type => $id_type,
				use_strand => $use_strand,
				conf => $conf,
				vals => $datas,
				label => 'norm Mean',
				probes_length => $probes_length);


	################################################
	print "generate the wig file\n"; ###############
	################################################
	#print "... skip\n";
	my $wig_file_fwd = $out_dir."/norm_mean_values_fwd.wig";
	my $wig_file_rev = $out_dir."/norm_mean_values_rev.wig";
	if($use_strand){
		# read the .pair template file, add values and generate the wig
		convertPairToWig(	pair_file => $template_file,
					wig_file_fwd => $wig_file_fwd,
					wig_file_rev => $wig_file_rev,
					id_type => $id_type,
					use_strand => $use_strand,
					vals => $datas,
					conf => $conf,
					type => 'wiggle_0',
					labelFWD => 'norm Mean FWD',
					labelREV => 'norm Mean REV',
					probes_length => '50',
					autoScale => 'off',
					viewLimits => '-2:9',
					visibility => 'full',
					yLineMark => '0',
					yLineOnOff => 'on',
					colorFWD => '',
					colorREV => '',
					priority => '10');
	}else{
		# read the .pair template file, add values and generate the wig
		convertPairToWig(	pair_file => $template_file,
					wig_file_fwd => $wig_file_fwd,
					id_type => $id_type,
					use_strand => $use_strand,
					vals => $datas,
					conf => $conf,
					type => 'wiggle_0',
					labelFWD => 'norm Mean FWD',
					probes_length => '50',
					autoScale => 'off',
					viewLimits => '-2:9',
					visibility => 'full',
					yLineMark => '0',
					yLineOnOff => 'on',
					colorFWD => '',
					colorREV => '',
					priority => '10');
	}
}else{
	if($id_type eq 'id'){
		$datas = getValues(	file => $gff_file,
					value_column => 6,
					id_column => 9,
					header => 1 );
	}else{
		open (IN, $gff_file) or die "sub getValues: Unable to open the file $template_file .\n";
		my %temp ;
		my $ct = -1;
		while (<IN>) {
			$ct++;
			next if($ct<1);
			chomp();
			my @line = split(/\t/);
			push(@{$temp{uc($line[0])."-".$line[3]}},$line[5]);
		}
		foreach my $k (keys(%temp)){
			my $val;
			foreach my $v(@{$temp{$k}}){
				$val =+ $v;
			}
			$$datas{$k} = $val/scalar(@{$temp{$k}});
		}
		close IN;
	}
}

# print Dumper $datas;
# exit 0;

# we recover the threshold value.
################################################
print "calcul threshold\n"; ####################
################################################
my $threshold = calcThreshold(	file_in => $gff_file,
					plot_file => $out_dir."/expression_thresold.pdf",
					temp_dir => $temp_dir,
					value_column => '6',
					header => 'TRUE',
					FDRthresh => $FDRthresh);

#######################################################################
print "generate the gff file with the over express probes\n"; #########
#######################################################################
my $over_express_probes_gff_file = $out_dir."/over_express_probes.gff";

#read the .pair template file, add values and generate the gff with the over express probes.
convertPairToGFF(	pair_file => $template_file,
			gff_file => $over_express_probes_gff_file,
			conf => $conf,
			vals => $datas,
			id_type => $id_type,
			use_strand => $use_strand,
			label => 'over express probes',
			probes_length => $probes_length,
			filter_value_min => $threshold);

##################################################################################
print "generate the wig file (fwd and rev) with the over express probes\n"; ######
##################################################################################
my $over_express_probes_wig_file_fwd = $out_dir."/over_express_probes_fwd.wig";
my $over_express_probes_wig_file_rev = $out_dir."/over_express_probes_rev.wig";
#print "...skip\n";
if($use_strand){
	#read the .pair template file, add values and generate the wig with the over express probes.
	convertPairToWig(	pair_file => $template_file,
				wig_file_fwd => $over_express_probes_wig_file_fwd,
				wig_file_rev => $over_express_probes_wig_file_rev,
				vals => $datas,
				conf => $conf,
				id_type => $id_type,
				use_strand => $use_strand,
				type => 'wiggle_0',
				labelFWD => 'over express probes FWD',
				labelREV => 'over express probes REV',
				probes_length => $probes_length,
				autoScale => 'off',
				viewLimits => '0:8',
				visibility => 'full',
				yLineMark => '0',
				yLineOnOff => 'on',
				colorFWD => '',
				colorREV => '',
				priority => '10',
				filter_value_min => $threshold);
}else{
	#read the .pair template file, add values and generate the wig with the over express probes.
	convertPairToWig(	pair_file => $template_file,
				wig_file_fwd => $over_express_probes_wig_file_fwd,
				vals => $datas,
				conf => $conf,
				id_type => $id_type,
				use_strand => $use_strand,
				type => 'wiggle_0',
				labelFWD => 'over express probes',
				probes_length => $probes_length,
				autoScale => 'off',
				viewLimits => '0:8',
				visibility => 'full',
				yLineMark => '0',
				yLineOnOff => 'on',
				colorFWD => '',
				colorREV => '',
				priority => '10',
				filter_value_min => $threshold);
}
#######################################
print "make segmentation\n"; ##########
#######################################
my %segmentsInfos = %{makeSegClustOnPair(	pair_file => $template_file,
						temp_dir => $temp_dir,
						vals => $datas,
						id_type => $id_type,
						conf => $conf,
						use_strand => $use_strand,
						group_seqs => $group_seqs,
						group_strands => $group_strand,
						max_seg_length => $max_seg_length,
						probes_length => $probes_length,
						Kmax => $Kmax,
						group_size => $group_size)};

########################################################################
print "generate the gff file with the segmentation's values\n"; ########
########################################################################
my $segment_values_gff_file = $out_dir."/segment_values.gff";

convertSegmentationResultsToGFF( 	segmentsInfos => \%segmentsInfos,
					gff_file => $segment_values_gff_file,
					label => 'Segment Value');

####################################################################################
print "generate the wig file (fwd and rev) with the segmentation's values\n"; ######
####################################################################################
my $segment_values_wig_file_fwd = $out_dir."/segment_values_fwd.wig";
my $segment_values_wig_file_rev = $out_dir."/segment_values_rev.wig";

if($use_strand){
	convertSegmentationResultsToWIG(	wig_file_fwd => $segment_values_wig_file_fwd,
						wig_file_rev => $segment_values_wig_file_rev,
						segmentsInfos => \%segmentsInfos,
						type => 'wiggle_0',
						labelFWD => 'Segment Value FWD',
						labelREV => 'Segment Value REV',
						autoScale => 'off',
						viewLimits => '-1:8',
						visibility => 'full',
						yLineMark => '0',
						yLineOnOff => 'on',
						colorFWD => '',
						colorREV => '',
						priority => '10');
}else{
	convertSegmentationResultsToWIG(	wig_file_fwd => $segment_values_wig_file_fwd,
						segmentsInfos => \%segmentsInfos,
						type => 'wiggle_0',
						labelFWD => 'Segment Value',
						autoScale => 'off',
						viewLimits => '-1:8',
						visibility => 'full',
						yLineMark => '0',
						yLineOnOff => 'on',
						colorFWD => '',
						colorREV => '',
						priority => '10');	
}

####################################################################################
print "generate the GFF file containing the over Expressed segments\n"; ############
####################################################################################

my $over_express_segment_values_gff_file = $out_dir."/over_express_segment_values.gff";

# Array of hash whish contain the condition for the different threshold for the applyThresoldsOnFile fonction.
my @thrds = ({	threshold_type => 'min',
		threshold_strict => 0,
		column => 5,
		value => $threshold});

#read the file in parameter and generate the over expressed value according to the different threshold in parameter in the GFF file out.
applyThresoldsOnFile( 	file_in => $segment_values_gff_file,
			file_out => $over_express_segment_values_gff_file,
			keep_header => 1,
			add_header => '',
			logical_operator_or => '',
			thresholds => \@thrds);








=begin function

Function : extractNormMeanValuesFromPair
Description : 	fonction qui va permettre dde traiter des fichiers Pair de la manière suivnate:
			- extratcion des données par fichier puis normalisation (log2 -> scale )
			- calcul de la moyenne des valeurs de tous els fichiers
Usage :	applyThresoldsOnFile( 	files => Table_ref : liste des paths fichiers à traiter,
				conf => Hash_ref : liste des confs (cf Conf.pm)
				id_type => String : 'loc' ou 'id' type d'indentifiant des probes
				inh_dir => String : path du repertoire contenat les fichiers d'entrée
);

Returns : \%means { probe_id => value }
Version : v1.0

=end function

=cut

sub extractNormMeanValuesFromPair { 
	
	my %args = @_;
	my @files = @{$args{files}};
	my $conf = $args{conf};
	my $id_type = $args{id_type};
	my $in_dir = $args{in_dir};
	my $use_strand = $args{use_strand};
	my $normalized_file;
	if($args{normalized_file}){
		$normalized_file = $args{normalized_file};
	}else{
		$normalized_file = $out_dir."/norm_values.csv";
	}
	my @ids;
	my $means;
	my %values;

	#############################################
	print "check pair file ".$files[0]."\n"; ####
	#############################################
	my $cmd  = "cut -f 2 ".$files[0]." | sort | uniq -c";
	my $awk = `$cmd`;
	print " -the folowing probe types have been found\n";
	print $awk;
	print " -> the folowing categories will be threated :\n";
	foreach my $k (keys(%{$$conf{good_cats}})){
		print "\t$k\n";
	}
	 "to make your own good probe type liste, please use the --probe_types parameter.\n";

	# foreach pair file
	for (my $i = 0 ; $i < scalar(@files); $i++){
		my $f = $files[$i];
		#############################################
		print "extract data from file $f\n"; ########
		#############################################
	# 	we retrieve the values
		my @sample = @{getValuesFromPairFile($in_dir,$f,$id_type,$use_strand,$conf)};
		if(scalar(@sample) == 0){
			die "can't extract values from the file $f\n";
		}
	# 	we add the values to the global values array
		@ids = @{addSampleToGroupe(\%values,\@sample)};
	}
	# create the file containing the raw values of each sample
	my $raw_file = createDatasetFile(\%values,\@ids,$temp_dir,\@files);

	#############################################
	print "normalization of datas\n"; ###########
	#############################################
	# create the file containing the normalized values of each sample
	normalizeDatas($raw_file,$normalized_file,$temp_dir,scalar(@files));
	# delete the raw file
# 	my $del = `rm -rf $raw_file`;

	# if there is only one file, we skip the mean claculation step
	if(scalar(@files)>1){
		################################################
		print "calcul mean for each probe\n"; ##########
		################################################

		# add to the file the mean value
		my $norm_mean_file = $out_dir."/norm_mean_values.csv";
		calcMean(	file_in => $normalized_file,
				file_out => $norm_mean_file,
				temp_dir => $temp_dir);
		
		# replace the old file by this news 
		my $mv = `mv $norm_mean_file $normalized_file`;

		# store the Mean Values
		$means = getValues(	file => $normalized_file,
					value_column => (scalar(@files)+2),
					id_column => 1,
					header => 1 );
	}else{
		$means = getValues(	file => $normalized_file,
					value_column => (scalar(@files)+1),
					id_column => 1,
					header => 1 );
	}
	
	return $means;
}

=begin function

Function : applyThresoldsOnFile
Description : 	fonction generique qui permet d'aplliquer un seuil à un fichier tabulé.
		fonction qui permet egalement de traiter les conditions en fonction de "et" (&&) ou "ou" (||) (default &&).
Usage :	applyThresoldsOnFile( 	file_in => String : Pth du fichier a trier,
				file_out => String : Pth du fichier GFF de sortie contenant les valeurs trier,
				keep_header => [number] Indique le nombre de lignes de header que le fichier contient. ce parametre peut prendre trois types de valeurs:
							0: le fichier ne contient pas de header
							+X: le fichier contient X lignes de header que l'on veut conserver
							-X: le fichier contient X lignes de header que l'on ne veut pas conserver,
				add_header => String : permet d'ajouter une ligne suplémentaire au header (cette ligne sera ajoutee apres les headers originaux),
				logical_operator_or => [number] Indique si on est en condition "et" ou "ou" pour le test des differentes conditions,
								undef : condition "et" (&&), toutes les conditions doivent etre respectees
								R* [number] : condition "ou" (||), une seule des conditions doit etre respectee pour printer la ligne.
				thresholds => );
Parameters :	thresholds =>	$VAR1 = {
						'threshold_type' => ,
						'value' => ,
						'threshold_strict' => ,
						'column' => 
					};

Returns : none
Version : v1.0

=end function

=cut

sub applyThresoldsOnFile {
	my %args = @_;
	my $file_in = $args{file_in};
	my $file_out = $args{file_out};
	my $keep_header = $args{keep_header};
	my $add_header = $args{add_header};
	my @thresholds = @{$args{thresholds}};

	my $i;

	open(IN,$file_in) or die "sub applyThresoldsOnFile: Unable to open the file $file_in .\n";
	open (OUT,">$file_out") or die "sub applyThresoldsOnFile: Unable to create the file $file_out .\n";

	# si on veux conserver plusieurs ligne de l'header du fichier d'entree.
	if($keep_header =~ /\+*([1-9])+/){
		$i = $1;
		# permet de conserver les header.
		for(my $j = 1; $j <= $i; $j++){
			my $ln = <IN>;
			print OUT "$ln";
		}
	# si on ne veux pas les headers du fichier d'entree.
	}elsif($keep_header =~ /\-(\d)+/){
		$i = $1;
		for(my $j = 1; $j <= $i; $j++){
			<IN>;
		}
	}
	# si on veux rajouter un header.
	if($add_header != ''){
		# ecrit le nouvel header.
		print OUT "$add_header\n";
	}

	# permet de changer de ligne.
	SHIFT:
	# pour toutes les lignes du fichier.
	while(<IN>){
		chomp;
		# on recupere la ligne complete.
		my $cur_line = $_;
		my @line = split(/\t/);
		# pour chaque condition.
		for(my $j ; $j < scalar(@thresholds); $j++){
			my $k = $thresholds[$j];
			# si le seuil est un max.
			if(uc($$k{threshold_type}) =~ /MAX/){
				# les valeurs triees seront inferieur ou egal au seuil.
				if($$k{threshold_strict} == 0){
					# si on est en conditon "ou".
					if($args{logical_operator_or} ne ''){
						# si la valeur est inferieur ou egal au seuil, alors on print la ligne et on change de ligne.
						if($line[$$k{column}] <= $$k{value}){
							print OUT "$cur_line\n";
							goto SHIFT;
						}
					# si la valeur est strictement superieur au seuil, on change de ligne.
					}elsif($line[$$k{column}] > $$k{value}){
						goto SHIFT;
					}
				# les valeurs triees seront strictement inferieur au seuil.
				}elsif($$k{threshold_strict} == 1){
					# si on est en conditon "ou".
					if($args{logical_operator_or} ne ''){
						# si la valeur est strictement inferieur au seuil, alors on print la ligne et on change de ligne.
						if($line[$$k{column}] < $$k{value}){
							print OUT "$cur_line\n";
							goto SHIFT;
						}
					# si la valeur est superieur ou egal au seuil, on change de ligne.
					}elsif($line[$$k{column}] >= $$k{value}){
						goto SHIFT;
					}
				}
			# si le seuil est un min.
			}elsif(uc($$k{threshold_type}) =~ /MIN/){
				# les valeurs triees seront superieur ou egal au seuil.
				if($$k{threshold_strict} == 0){
					# si on est en conditon "ou".
					if($args{logical_operator_or} ne ''){
						# si la valeur est superieur ou egal au seuil, alors on print la ligne et on change de ligne.
						if($line[$$k{column}] >= $$k{value}){
							print OUT "$cur_line\n";
							goto SHIFT;
						}
					# si la valeur est strictement inferieur au seuil, on change de ligne.
					}elsif($line[$$k{column}] < $$k{value}){
						goto SHIFT;
					}
				# les valeurs triees seront strictement superieur au seuil.
				}elsif($$k{threshold_strict} == 1){
					# si on est en conditon "ou".
					if($args{logical_operator_or} ne ''){
						# si la valeur est strictement superieur au seuil, alors on print la ligne et on change de ligne.
						if($line[$$k{column}] < $$k{value}){
							print OUT "$cur_line\n";
							goto SHIFT;
						}
					# si la valeur est inferieur ou egal au seuil, on change de ligne.
					}elsif($line[$$k{column}] >= $$k{value}){
						goto SHIFT;
					}
				}
			# si les valeurs doivent être egal au seuil.
			}elsif(uc($$k{threshold_type}) =~ /EGAL/){
				# si on est en conditon "ou".
				if($args{logical_operator_or} ne ''){
					# si la valeur remplie la condition "egal", alors on print la ligne et on change de ligne.
					if($line[$$k{column}] == $$k{value}){
						print OUT "$cur_line\n";
						goto SHIFT;
					}
				# on change de ligne si la valeur est differente du seuil.
				}elsif($line[$$k{column}] != $$k{value}){
					goto SHIFT;
				}
			# si le seuil est un string.
			}elsif((uc($$k{threshold_type}) =~ /\w*/) && !(uc($$k{threshold_type}) =~ /MAX/) && !(uc($$k{threshold_type}) =~ /MIN/) && !(uc($$k{threshold_type}) =~ /EGAL/)){
				# si on est en conditon "ou".
				if($args{logical_operator_or} ne ''){
					# si la valeur remplie la condition "string", alors on print la ligne et on change de ligne.
					if($line[$$k{column}] eq $$k{value}){
						print OUT "$cur_line\n";
						goto SHIFT;
					}
				# on change de ligne si la valeur est differente du string.
				}elsif($line[$$k{column}] ne $$k{value}){
					goto SHIFT;
				}
			}
		}
		# si on est en condition "et"/"&&", alors on print les lignes d'interets.
		if($args{logical_operator_or} eq ''){
			# on ecrit la ligne dans le fichier de sorti.
			print OUT "$cur_line\n";
		}
	}
	close IN;
	close OUT;
}

=begin function

Function : printWIGSegment
Description : cette fonction permet de printer les valeurs des segments en fonction de FWD ou REV et avec le bon header correspondant.
Usage : printWIGSegment(	segmentsInfos => ,
				track_desc => String: header du fichier creer avec la fonction setTrackDescription,
				file_out => String: Pth du fichier wig de sortie (fwd ou rev));
Parameters : 	segmentsInfos => VAR$1 = {
						'ProbeID' {
								start => ,
								value => ,
								strand => ,
								seq => );
							}
					}
Returns : none
Version : v1.0

=end function

=cut

sub printWIGSegment{
	my %args = @_;
	my @segmentsInfos = @{$args{segmentsInfos}};
	my $track_desc = $args{track_desc};
	my $file_out = $args{file_out};

	my $seq;

	open (OUT,">$file_out") or die "sub printWIGSegment: Unable to create the file $file_out .\n";
	# pour chaque segment
	for(my $j ; $j < scalar(@segmentsInfos); $j++){
		my $seg = $segmentsInfos[$j];
		# on inscrit le premier header.
		if($seq eq ''){
			$seq = $$seg{seq};
			print OUT $track_desc."\n";
			print OUT "variableStep chrom=".$$seg{seq}."\n";
		# si on change de seq, l'header est modifie.
		}elsif($seq != $$seg{seq}){
			$seq = $$seg{seq};
			print OUT $track_desc."\n";
			print OUT "variableStep chrom=".$$seg{seq}."\n";
		}
		# pour chaque position du segment
		for(my $i = $$seg{start}; $i <= $$seg{stop}; $i++){
			# si la position du segment courant devient egal au start du segment suivant, alors on change de segment.
			if($j < (scalar(@segmentsInfos)-1 ) && $i == $segmentsInfos[$j+1]{start}){
				last;
			}
			# on ecrit la valeur pour chaque position du segment.
			print OUT $i."\t".$$seg{val}."\n";
		}
	}
	close OUT;
# 	print "end of printWIGSegment \n";
}

=begin function

Function : IntegrateSegmentationValueInToWigFile
Description : 	permet de generer un fichier Wig avec les informations sur les segments precedement calcule.
		plusieurs options sont à définir, elles permettent de fixer :
 			le centre automatiquement(autoScale->on/off) ou manuellement(viewLimits->min:max),
 			la couleur de la courbe resultat(color->RRR,GGG,BBB),
			la visibilité de la courbe(visibility->full/dense/hide),
 			la valeur min de y pour défnir une valeur seuil(yLineMark->on/off),
			la visibilité de la ligne y(yLineOnOff->on/off),
			la prorité(priority->N)	
Usage :IntegrateSegmentationValueInToWigFile(	wig_file => String: Pth du fichier wig de sortie,
						segmentsInfos =>,
						type => ,
						labelFWD => ,
						labelREV => ,
						autoScale => ,
						viewLimits => ,
						visibility => ,
						yLineMark => ,
						yLineOnOff => ,
						colorFWD => ,
						colorREV => ,
						priority => );
Parameters :	segmentsInfos => VAR$1 = {
						'Strand' {
								'ProbeID' {
										start => ,
										value => ,
										strand => ,
										seq => );
									}
							}
					}
Returns : none
Version : v1.0

=end function

=cut

sub convertSegmentationResultsToWIG {
	my %args = @_;
	my $wig_file_fwd = $args{wig_file_fwd};
	my $wig_file_rev = $args{wig_file_rev};
	my %segmentsInfos = %{$args{segmentsInfos}};
	my @segmentsInfos_fwd;
	my @segmentsInfos_rev;

	foreach my $k (keys(%segmentsInfos)){
		if (uc($k) =~ m/FORWARD/ || uc($k) =~ m/FWD/ || uc($k) eq '+'){
			@segmentsInfos_fwd = @{$segmentsInfos{$k}};
		}
		else{
			@segmentsInfos_rev = @{$segmentsInfos{$k}};
		}
	}

	# appel de la fonction permettant la construction du header en fonction de FWD ou REV.
	my $track_FWD_args = setTrackDescription(	attributs => \%args,
							strand => 'fwd' );

	my $track_REV_args = setTrackDescription(	attributs => \%args,
							strand => 'rev' );

	# on print dans le fichier le bon header et les donnees en fonction de FWD ou REV.
	printWIGSegment(	segmentsInfos=> \@segmentsInfos_fwd,
				track_desc => $track_FWD_args,
				file_out => $args{wig_file_fwd});
	if(scalar(@segmentsInfos_rev)>0){
		printWIGSegment(	segmentsInfos => \@segmentsInfos_rev,
					track_desc => $track_REV_args,
					file_out => $args{wig_file_rev});
	}

}

=begin function

Function : IntegrateSegmentationValueInToGFFFile
Description : permet de genrerer un fichier GFF avec les informations sur les segments precedement calcule.
Usage : IntegrateSegmentationValueInToGFFFile( 	segmentsInfos => ,
						gff_file => String : path du fichier GFF genere en sortie,
						label => String : label des valeurs traitees);
Parameters :	segmentsInfos => VAR$1 = {
						'Strand' {
								'ProbeID' {
										start => ,
										value => ,
										strand => ,
										seq => );
									}
							}
					}
Returns : none
Version : v1.0

=end function

=cut

sub convertSegmentationResultsToGFF {
	my %args = @_;
	my $gff_file = $args{gff_file};
	my %segmentsInfos = %{$args{segmentsInfos}};
	my $label = $args{label};

	open (OUT,">$gff_file") or die "sub convertSegmentationResultsToGFF: Unable to create the file $gff_file .\n";
	print OUT "##gff-version	3\n";
	foreach my $k (keys(%segmentsInfos)){
		if (uc($k) =~ m/FORWARD/ || uc($k) =~ m/FWD/ || uc($k) eq '+'){
			foreach my $seg (@{$segmentsInfos{$k}}){
				print OUT $$seg{seq}."\tTiling Array\t".$label." FWD\t".$$seg{start}."\t".$$seg{stop}."\t".$$seg{val}."\t+\t.\t".$$seg{segID}."\n";
			}
		}else{
			foreach my $seg (@{$segmentsInfos{$k}}){
				print OUT $$seg{seq}."\tTiling Array\t".$label." REV\t".$$seg{start}."\t".$$seg{stop}."\t".$$seg{val}."\t-\t.\t".$$seg{segID}."\n";
			}
		}
	}
	close OUT;
}

=begin function

Function : getSegmentInfo
Description : 	fonction qui permet de recuperer les starts, stops et valeurs des segments.
		Elle permet egalement de differencier plusieur segments ayant la même valeur.
Usage : %{getSegmentInfo(	file => String :  path du fichier temporaire contenant les valeurs des segments,
				probes_length => String : taille des probes,
				probes_infos => ;
Parameters :	probes_infos => VAR$1 = {
						'ProbeID' {
								start => ,
								value => ,
								strand => ,
								seq => );
							}
					}
Returns : 	{	rev => \@segment_rev,
			fwd => \@segment_fwd};
Version : v1.0

=end function

=cut
sub getSegmentInfo {
	my %args = @_;
	my $file = $args{file};
	my $probes_length = $args{probes_length};
	my %probes_infos = %{$args{probes_infos}};

	my @segment_fwd;
	my @segment_rev;
	my $val;
	my $start;
	my $stop;
	my $strand;
	my $seq;
	my @line;

	open(IN,$file) or die "sub segmentInfo: Unable to open the file $file .\n";
	<IN>;
	while (<IN>){
		# on definit les arguments des variables courantes start, stop et la valeur.
		chomp();
		@line = split(/\t/);
		$line[0] =~ s/"//g;
		my $cur_val = $line[1];
		my $cur_start = $probes_infos{$line[0]}{start};
		my $cur_stop = $cur_start + $probes_length - 1;
		my $cur_strand = $probes_infos{$line[0]}{strand};
		my $cur_seq = $probes_infos{$line[0]}{seq};
		my $add_seg;

		# si on est à la premiere ligne du fichier -> on ne fait rien a part l'initialisation des valeurs courantes
		if($val ne ''){
			# on test si la strand precedente est differente de la strand courante => on change de segment.
			if(uc($strand) ne uc($cur_strand)){
				$add_seg =1;
			# on test si la seq precendente est differente de la seq courante => on change de segment.
			}elsif(uc($seq) ne uc($cur_seq)){
				$add_seg =1;
			# on test si la valeur precedente est egale a la valeur courante (meme segment).
			}elsif($val == $cur_val){
				# si le stop de la probes precedente est superieur au start de la probe courante (chevauchement).
				if($stop > $cur_start){
					$stop = $cur_stop;
					next;
				# si le stop de la probe precedente est inferieur au start de la probe courante (cassure dans le segment mais meme valeur).
				}else{
					# on rentre les informations du segment precedent avant de demarrer sur un nouveau segment de meme valeur.
					$add_seg =1;
				}
			# si la valeur precedente n'est pas egale avec la valeur courante, alors on est sur un autre segment.
			}else{
				# on rentre les informations du segment precedent avant de demarrer sur un nouveau segment de meme valeur.
				$add_seg =1;
			}
		}
		if($add_seg){
			my $name = $probes_infos{$line[0]}{seq}."_".$strand.":".$start."-".$stop;
			my %seg = (	val => $val,
					start => $start,
					stop => $stop,
					strand => $strand,
					seq => $probes_infos{$line[0]}{seq},
					segID => $name);
			if($strand eq '+' || uc($strand) =~ m/FWD/ || uc($strand) =~ m/FORWARD/){
				push(@segment_fwd,\%seg);
			}else{
				push(@segment_rev,\%seg);
			}
		}
		# on recupere les valeurs courantes
		$val = $cur_val;
		$start = $cur_start;
		$stop = $cur_stop;
		$strand = $cur_strand;
		$seq = $cur_seq;
	}
	# on rentre les informations du dernier segment 
	my $name = $probes_infos{$line[0]}{seq}."_".$strand.":".$start."-".$stop;
	my %seg = (	val => $val,
			start => $start,
			stop => $stop,
			strand => $strand,
			seq => $probes_infos{$line[0]}{seq},
			segID => $name);
	if($strand eq '+' || uc($strand) =~ m/FWD/ || uc($strand) =~ m/FORWARD/){
		push(@segment_fwd,\%seg);
	}else{
		push(@segment_rev,\%seg);
	}
	close(IN);
	return {	rev => \@segment_rev,
			fwd => \@segment_fwd};
}

=begin function

Function : makeSegClust
Description : 	fonction qui va lancer la segmentation sur les données disponibles dans le fichier tabulé passé en argument.
		la methode de segmentation utilisee sera segmean du package R segClust.
		les regions sur lesquelles sont calculées les segments sont couvrantes au 4/5eme.
Usage : %{makeSegClust(	file => ,
			temp_dir => string : path du fichier contenant les fichier pair,
			id_column => String : ,
			value_column => String : nombre servant de reference pour la colone a selectionne dans le script R,
			header=> String : ,
			probes_infos => ,
			probes_length => String : taille des probes,
			max_seg_length => String : longueur maximum des probes,
			group_size => String : taille du groupe)};
Parameters :	probes_infos => VAR$1 = {
						'ProbeID' {
								start => $line[4],
								value => $value,
								strand =>$line[1],
								seq => $seq);
							}
					}
Returns :	\%segments => VAR$1 = {
						'Strand' {
								'ProbeID' {
										start => ,
										value => ,
										strand =>,
										seq => );
									}
							}
					}
Version : v1.0

=end function

=cut
sub makeSegClust {
	my %args = @_;
	my $file = $args{file};
	my $id_col = $args{id_column};
	my $val_col = $args{value_column};
	my $max_seg_length = $args{max_seg_length};
	my $Kmax = $args{Kmax};
	my $group_size = $args{group_size};
	my $header = $args{header};
	my $temp_dir = $args{temp_dir};

	if(!-f($file)){
		die "sub makeSegClust: unable to open file $file.\n";
	}
	if($args{group_size} < 1){
		die "sub makeSegClust: group_size parameter is not valid.\n";
	}
	if(!$args{id_column}){
		die "sub makeSegClust: id_column parameter is not valid.\n";
	}
	if(!$args{value_column}){
		die "sub makeSegClust: value_column parameter is not valid.\n";
	}
	if(!$args{max_seg_length}){
		$max_seg_length = 1000;
	}
	if(!$temp_dir){
		$temp_dir = "./";
	}
	if(!$Kmax){
		$Kmax = '(length(vals)/10)';
	}
	
	my $r_script = $file."_r_script.r";
	my $seg_res = $file."_r_res.res";

	print " |-create R script\n";	
	# on va rediger le fichier R qui sera utilisé pour calculer les segments.
	open (R_file, ">".$r_script);
	# on se place au bon endroit.
	print R_file "setwd(\"".$temp_dir."\")\n";
	# on lit le fichier contenat les valeurs.
	if($header){
		print R_file "values=read.csv(\"".$file."\",header=TRUE,sep=\"\\t\")\n\n";
	}else{
		print R_file "values=read.csv(\"".$file."\",sep=\"\t\")\n\n"
	}
	print R_file "# on charge la librarie.\n";
	print R_file "library(\"segclust\")\n\n";

	print R_file "# on compte le nombre de valeurs du fichier.\n";
	print R_file "nb_vals = length(values\$ID)\n";
	print R_file "max=".$args{group_size}."\n";
	print R_file "segs_values = c(1)\n\n";

	print R_file "# si le nombre de valeur est plus faible que la taille du groupe de probes defini.\n";
	print R_file "if(max>=nb_vals){\n";
	print R_file "	vals = values\$VALUE\n";
	print R_file "	# permet la segmentation.\n";
	print R_file "	out_segs=segmean(vals,Kmax=".$Kmax.",lmax=".$max_seg_length.",vh=TRUE)\n";
	print R_file "	Kselect_segs=segselect(out_segs\$J.est,Kmax=".$Kmax.")\n";
	print R_file "	output_segs=segout(vals,K=Kselect_segs,th=out_segs\$t.est,draw=FALSE)\n";
	print R_file "	# remplie dans segs_values toutes les segments generes.\n";
	print R_file "	for (i in 1:length(output_segs\$mean)){\n";
	print R_file "		segs_values[i] = output_segs\$mean[i]\n";
	print R_file "	}\n";

	print R_file "# si le nombre de valeur est supérieur a la taille du groupe de probes defini.\n";
	print R_file "}else{\n";
	print R_file "	# on calcul un pas.\n";
	print R_file "	pas = (max\%/\%5)*4\n";
	print R_file "	starts=c(1)\n";
	print R_file "	i=1\n";
	print R_file "	# on calcul toutes les positions de départ grace au pas.\n";
	print R_file "	# si le dernier pas nous fait sortir on recherche la derniere position permettant de faire un pas.\n";
	print R_file "	while(i>0){\n";
	print R_file "		# si le fait d'ajouter un pas nous fait sortir\n";
	print R_file "		if((starts[length(starts)]+ max) > nb_vals){\n";
	print R_file "			starts[length(starts)] = nb_vals - max +1\n";
	print R_file "			i = 0\n";
	print R_file "		}else{\n";
	print R_file "			starts[length(starts)+1]= starts[length(starts)]+ pas\n";
	print R_file "		}\n";
	print R_file "	}\n";
	print R_file "	# pour toute les positions de départ calculées.\n";
	print R_file "	for (i in 1:length(starts)){\n";
	print R_file "		# on selectionne toutes les valeurs allant d'une position de départ à la position de départ + le max (et -1, pour prévoir le bon départ suivant).\n";
	print R_file "		vals = values\$VALUE[starts[i]:(starts[i]+max-1)]\n";
	print R_file "		# permet la segmentation.\n";
	print R_file "		out_segs=segmean(vals,Kmax=".$Kmax.",lmax=".$max_seg_length.",vh=TRUE)\n";
	print R_file "		Kselect_segs=segselect(out_segs\$J.est,Kmax=".$Kmax.")\n";
	print R_file "		output_segs=segout(vals,K=Kselect_segs,th=out_segs\$t.est,draw=FALSE)\n";
	print R_file "		shift=0\n";
	print R_file "		# pour chaques probes\n";
	print R_file "		for (j in 0:(max-1)){\n";
	print R_file "			# si il s'agit du premier groupe de probe, on prend toutes les valeurs.\n";
	print R_file "			if(i==1){\n";
	print R_file "				segs_values[(starts[i]+j)] = output_segs\$mean[j+1]\n";
	print R_file "			# sinon on compare les valeurs entre elles pour tester plusieurs situations.\n";		
	print R_file "			}else{\n";
	print R_file "				# si on se trouve à la fin du groupe de probes actuel, on shift sur les valeurs d'un nouveau groupe de probes.\n";
	print R_file "				if(j == (starts[i-1]+max-starts[i]-1)){\n";
	print R_file "					shift =1\n";
	print R_file "				}\n";
	print R_file "				# si on a shifté, on passe les valeurs du nouveau groupe de probes dans le tableau segs_values.\n";
	print R_file "				if(shift == 1){\n";
	print R_file "					segs_values[(starts[i]+j)] = output_segs\$mean[j+1]\n";
	print R_file "				# si on est dans la cas d'une zone de chevauchement.\n";
	print R_file "				}else{\n";
	print R_file "					# si les valeurs du nouveau groupes de probes sont egales à celles de l'ancien, alors on conserve la valeur de l'ancien groupe de probes.\n";
	print R_file "					if(segs_values[(starts[i]+j)] == output_segs\$mean[j+1]){\n";
	print R_file "						shift=1\n";
	print R_file "					}\n";
	print R_file "				}\n";
	print R_file "			}\n";
	print R_file "		}\n";
	print R_file "	}\n";
	print R_file "}\n";

	print R_file "# on remplace les valeurs de départ par les valeurs des segments.\n";
	print R_file "end_values = values\n";
	print R_file "end_values[,2]=segs_values\n";
	
	print R_file "write.table(end_values,\"".$seg_res."\",sep=\"\\t\",row.names=FALSE,col.names=TRUE)\n";
	
	close R_file;
	print " |-run R script\n";

	# lancement du script R.
	runR->lunchRScript($r_script);

	print " |-end of Segmentation step\n";

	my %segment = %{getSegmentInfo(	file => $seg_res,
					probes_length => $args{probes_length},
					probes_infos => $args{probes_infos})};
				
	# on efface les fichiers temporaires.
# 	`rm -rf $r_script`;
#	`rm -rf $seg_res`;

	return \%segment;

}

=begin function

Function : makeSegClustOnPair
Description :	fonction qui va lancer la segmentation sur les données disponibles dans le fichier .Pair passé en argumant.
		la methode de segmentation utilisee sera segmean du package R segClust.
		si l'attribut vals est renseigné, alors ce sont ces valeurs qui seront utiliseés pour la segmentation, Le fichier pair ne servira alors que de template.
		group_seqs -> les valeurs des probes sont regroupées et analysées en meme temps quelque soient leurs séqeunces d'origine (ex chr1 et chr3, etc...).
		group_strand -> les valeurs des probes sont regroupées et analysées en meme temps quelque soient leurs strands d'origine (forward et reversre).
Usage : %{makeSegClustOnPair(	pair_file => String : tableau des path des fichier pair,
				temp_dir => string : path du fichier contenant les fichier pair,
				vals => ,
				group_seqs => String : option,
				group_strands => String : option,
				max_seg_length => String : ,
				probes_length => String : taille des probe,
				group_size => String : taille des groupe)};
Parameters :	vals => $VAR1 = {
					'ProbeID' => 'Value',
				}
Returns :	\%segments => VAR$1 = {
						'Strand' {
								'ProbeID' {
										start => ,
										value => ,
										strand =>,
										seq => );
									}
							}
					}
Version : v1.0

=end function

=cut
sub makeSegClustOnPair {
	my %args = @_;
	my $pair_file = $args{pair_file};
	my $vals = $args{vals};
	my $group_seqs = $args{group_seqs};
	my $group_strands = $args{group_strands};
	my $probes_length = $args{probes_length};
	my $max_seg_length = $args{max_seg_length};
	my $id_type = $args{id_type};
	my $use_strand = $args{use_strand};
	my $temp_dir = $args{temp_dir};
	
	if(my $probes_length == 0 || $probes_length eq ''){
		$probes_length = 50;
	}

	# on recupere les infos des probes (ID,start,value)
	my %probes_infos = %{getProbesInfos(	pair_file => $pair_file,
						conf => $args{conf},
						id_type => $id_type,
						use_strand => $use_strand,
						vals => $args{vals})};

# 	print Dumper(\%probes_infos);
# 	exit 0;

	my %segments;

	# on genere les differents groupes de probes (en fonction de la sequence et de la strand)
	my %probes_groups;
	foreach my $k (keys(%probes_infos)){
		# si c'est des randoms
		next if($probes_infos{$k}{start} == '0');
		# on identifie si c'est forward ou reverse.
		if($probes_infos{$k}{strand} =~ m/FORWARD/ || $probes_infos{$k}{strand} =~ m/FWD/ || $probes_infos{$k}{strand} =~ m/\+/){
			my @infos = ($k,$probes_infos{$k}{start},$probes_infos{$k}{value});
			push(@{$probes_groups{$probes_infos{$k}{seq}}{fwd}},\@infos);
		}else{
			my @infos = ($k,$probes_infos{$k}{start},$probes_infos{$k}{value});
			push(@{$probes_groups{$probes_infos{$k}{seq}}{rev}},\@infos);
		}
	}

	# maintenant on va trier chaque tableau par position croissante.
	foreach my $seq (keys(%probes_groups)){
		my @fwd = sort par_pos_asc @{${$probes_groups{$seq}}{fwd}};
		$probes_groups{$seq}{fwd} = \@fwd;
		if($use_strand){
			my @rev = sort par_pos_asc @{${$probes_groups{$seq}}{rev}};
			$probes_groups{$seq}{rev} = \@rev;
		}
	}

	# on va maintennat creer des fichiers selon les groupements choisits.
	my @files_liste;
	if($group_seqs eq 'yes'){
		# cas ou on groupe tout -> un seul fichier.
		# on groupe les sequences par strand et on enchaine les 2 strands.
		if($group_strands eq 'yes'){ 
			my $out_file = $temp_dir."/temp_seg.csv";
			open OUT , ">".$out_file;
			print OUT "ID\tVALUE\n";
			# on ecrit les forwards.
			foreach my $seq (keys(%probes_groups)){
				my @probes = @{${$probes_groups{$seq}}{fwd}};
				foreach my $inf (@probes){
					print OUT @{$inf}[0]."\t".@{$inf}[2]."\n";
				}
			}
			if($use_strand){
				# on ecrit les reverses.
				foreach my $seq (keys(%probes_groups)){
					my @probes = @{${$probes_groups{$seq}}{rev}};
					foreach my $inf (@probes){
						print OUT @{$inf}[0]."\t".@{$inf}[2]."\n";
					}
				}
			}
			close OUT;
			push(@files_liste,$out_file);
		# cas ou on fait un fichier par stand -> un fichier par strand.
		}else{
			my $out_file = $temp_dir."/temp_seg_fwd.csv";
			open OUT , ">".$out_file;
			print OUT "ID\tVALUE\n";
			# on ecrit les forwards.
			foreach my $seq (keys(%probes_groups)){
				my @probes = @{${$probes_groups{$seq}}{fwd}};
				foreach my $inf (@probes){
					print OUT @{$inf}[0]."\t".@{$inf}[2]."\n";
				}
			}
			close OUT;
			push(@files_liste,$out_file);
			if($use_strand){
				$out_file = $temp_dir."/temp_seg_rev.csv";
				open OUT , ">".$out_file;
				print OUT "ID\tVALUE\n";
				# on ecrit les reverses.
				foreach my $seq (keys(%probes_groups)){
					my @probes = @{${$probes_groups{$seq}}{rev}};
					foreach my $inf (@probes){
						print OUT @{$inf}[0]."\t".@{$inf}[2]."\n";
					}
				}
				close OUT;
				push(@files_liste,$out_file);
			}
		}
	}else{
		if($group_strands eq 'yes'){
			# on ecrit les forwards et les reverses dans le meme fichier -> nb fihiers = nb sequences.
			foreach my $seq (keys(%probes_groups)){
				my $out_file = $temp_dir."/temp_seg_".$seq.".csv";
				open OUT , ">".$out_file;
				print OUT "ID\tVALUE\n";
				my @probes_fwd = @{${$probes_groups{$seq}}{fwd}};
				foreach my $inf (@probes_fwd){
					print OUT @{$inf}[0]."\t".@{$inf}[2]."\n";
				}
				if($use_strand){
					my @probes_rev = @{${$probes_groups{$seq}}{fwd}};
					foreach my $inf (@probes_rev){
						print OUT @{$inf}[0]."\t".@{$inf}[2]."\n";
					}
				}
				close OUT;
				push(@files_liste,$out_file);
			}
		}else{
			# on divise tout -> nb fihier = nb sequences * 2.
			foreach my $seq (keys(%probes_groups)){
				my $out_file = $temp_dir."/temp_seg_".$seq."_fwd.csv";
				open OUT , ">".$out_file;
				print OUT "ID\tVALUE\n";
				my @probes_fwd = @{${$probes_groups{$seq}}{fwd}};
				foreach my $inf (@probes_fwd){
					print OUT @{$inf}[0]."\t".@{$inf}[2]."\n";
				}
				close OUT;
				push(@files_liste,$out_file);
				if($use_strand){
					$out_file = $temp_dir."/temp_seg_".$seq."_rev.csv";
					open OUT , ">".$out_file;
					print OUT "ID\tVALUE\n";
					my @probes_rev = @{${$probes_groups{$seq}}{fwd}};
					foreach my $inf (@probes_rev){
						print OUT @{$inf}[0]."\t".@{$inf}[2]."\n";
					}
					close OUT;
					push(@files_liste,$out_file);
				}
			}
		}
	}

	# on a maintenant la liste des fichiers contenant les informations.
	# pour chaque fichier on va calculer les segments.
	foreach my  $file (@files_liste){
		%segments = %{makeSegClust(	file => $file,
						temp_dir => $temp_dir,
						id_column => 1,
						value_column => 2,
						header=>1,
						probes_infos => \%probes_infos,
						probes_length => $args{probes_length},
						max_seg_length => $max_seg_length,
						Kmax => $args{Kmax},
						group_size => $args{group_size})};
	}

	return \%segments;
}

=begin function

Function : getProbesInfos
Description :	fonction qui va prendre en entrée un fichier pair, et renvoyer un hash contenant:
		en clef l'identifiant de la probe et en valeur la réference d'un hash contenant les Keys/values suivantes : start, value, strand, seq
Usage : %{getProbesInfos(	pair_file => String : tableau des path des fichier pair,
				vals => )};
Parameters :	vals =>$VAR1 = {
					'ProbeID' => 'Value',
				}
Returns :	%probes_infos => VAR$1 = {
						'ProbeID' {
								start => ,
								value => ,
								strand =>,
								seq => );
							}
					}
Version : v1.0

=end function

=cut
sub getProbesInfos{
	my %args = @_;
	my $pair_file = $args{pair_file};	
	my $vals = $args{vals};
	my %conf = %{$args{conf}};
	my $use_strand = $args{use_strand};
	my $id_type = $args{id_type};	
	my $good_cats = $conf{good_cats};
	my $import_values;
	
# 	print Dumper $vals;

	if(ref($vals) eq 'HASH'){
		$import_values = 1;
	}

	my %probes_infos;
	open (IN,$pair_file) or die "sub getProbesInfos: Unable to open the file $pair_file .\n";
	# remove headers
	for (my $i = 0; $i <  $$conf{nb_headers}; $i++){
		<IN>;
	}
	while(<IN>){
		chomp();
		my @line = split(/\t/);
		# next if the Probe Type is not supported
		next if !$$conf{good_cats}{@line[$$conf{categorie_col}-1]};
		# identification de la sequence.
		$line[$conf{seq_col}-1] =~ m/^([^\s]*)(:\d+-.+)*$/;
		my $strand = $line[$conf{strand_col}-1];
		my $seq = $1;
		next if !$$good_cats{@line[$conf{categorie_col}-1]};
		# make the probe_id 
		my $strand ;
		if($use_strand){
			$strand = getProbeStrandFromPair(	conf => $conf,
								line => \@line);
		}else{
			$strand = 'FWD';
		}
		my $probe_id = getProbeIDFromPair(	conf => $conf,
							strand => $strand,
							line => \@line,
							use_strand => $use_strand,
							id_type => $id_type);
		my $value;

# 		print "probe id : $probe_id value : ".$$vals{$probe_id}."\n";

		if($import_values){
			$value = $$vals{$probe_id};
		}else{
			$value = $line[$conf{value_col}-1];
		}
		my %temp = (	start => $line[$conf{position_col}-1],
				value => $value,
				strand => $strand,
				seq => $seq);
		next if(!$use_strand && $probes_infos{uc($probe_id)});
		$probes_infos{uc($probe_id)} = \%temp;
	}
	close IN;
	return \%probes_infos;
}

=begin function

Function : convertPairToGFF
Description :	fonction qui va convertir le fichier pair passer en argumant en fichier GFF.
		si un hash de valeurs est passé en argumant, alors ces valeurs seront associées.
		si un seuil est fourni en parametre, alors cette fonction peut directement genere un fichier wig avec les probes seulement exprime.
Usage :	convertPairToGFF(	pair_file => String : tableau des path des fichier pair,
				gff_file => String : path du fichier GFF genere en sortie,
				vals => ,
				label => String : label des valeurs traitees,
				probes_length => string : taille des probes,
				filter_value_min => String : valeur calcule à l'aide de la fonction calcThreshold);
Parameters :	vals => $VAR1 = {
					'ProbeID' => 'Value',
				}
Returns : none
Version : v1.0

=end function

=cut
sub convertPairToGFF{
	my %args = @_;
	my $pair_file = $args{pair_file};
	my %conf = %{$args{conf}};
	my $gff_file = $args{gff_file};
	my $vals = $args{vals};
	my $label = $args{label};
	my $id_type = $args{id_type};
	my $use_strand = $args{use_strand};
	my $probes_length = $args{probes_length};
	my $nb_controls;
	my $nb_probes_fwd;
	my $nb_probes_rev;
	my %view;
	my $good_cats = $conf{good_cats};
	if($probes_length == 0){
		$probes_length = 60;
	}
	my $import_values;
	if(ref($vals) eq 'HASH'){
		$import_values = 1;
	}

	open (IN,$pair_file) or die "sub convertPairToGFF: Unable to open the file $pair_file .\n";
	open (OUT,">$gff_file") or die "sub convertPairToGFF: Unable to create the file $gff_file .\n";
	# remove twe two first lines.
	<IN>;
	print OUT "##gff-version	3\n";
	<IN>;
	while (<IN>){
		chomp();
		my @line = split(/\t/);
		# identification de la sequence.
		$line[$conf{seq_col}-1] =~ m/^([^\s]*):\d+-.+$/;
		my $seq = $1;
		# si pas de sequence identifiee on est en random#!.
		if(!$seq){
			$nb_controls++;
			next;
		}
		next if !$$good_cats{@line[$conf{categorie_col}-1]};
		# make the probe_id 
		my $strand = getProbeStrandFromPair(	conf => $conf,
							line => \@line);
		my $probe_id = getProbeIDFromPair(	conf => $conf,
							strand => $strand,
							line => \@line,
							use_strand => $use_strand,
							id_type => $id_type);
		# on recupere la valeur si besoin.
		my $value;

		if($import_values){
			$value = $$vals{$probe_id};
			next if !$value;
		}else{
			$value = $line[$conf{value_col}-1];
		}
		# si on a deja vu cet id, alors on pass
		next if ($view{$probe_id});
		$view{$probe_id} = 1;

		# si on a definit des seuils de filtrage max ou min et que les valeurs les depassent, on ne les prend pas.
		if(($args{filter_value_min} && ($value <= $args{filter_value_min})) ||($args{filter_value_max} && ($value >= $args{filter_value_max}))){
			next;
		}else{
			if($use_strand){
				# on identifie si c'est fwd ou reverse.
				if($strand eq 'FWD'){
						print OUT $seq."\tTiling Array\t".$label." FWD\t".$line[$conf{position_col}-1]."\t".($line[$conf{position_col}-1]+$probes_length-1)."\t".$value."\t+\t.\t".$line[$conf{probe_id_col}-1]." X=".$line[5]." Y=".$line[6]."\n";
				}else{
						print OUT $seq."\tTiling Array\t".$label." REV\t".$line[$conf{position_col}-1]."\t".($line[$conf{position_col}-1]+$probes_length-1)."\t".$value."\t-\t.\t".$line[$conf{probe_id_col}-1]." X=".$line[5]." Y=".$line[6]."\n";
				}
			}else{
				print OUT $seq."\tTiling Array\t".$label."\t".$line[$conf{position_col}-1]."\t".($line[$conf{position_col}-1]+$probes_length-1)."\t".$value."\t+\t.\t".$line[$conf{probe_id_col}-1]." X=".$line[5]." Y=".$line[6]."\n";
			}
		}
		
	}
	close IN;
	close OUT;
}

=begin function

Function : setTrackDescription
Description :	cette fonction permet de construire le header d'un fichier au format wig en fonction des donnee FWD ou REV.
		le header sera egalement modifie en fonction des parametre passe ou non.
		cette fonction test egalement le format des attributs et ainsi verifier leur conformité.
Usage :	setTrackDescription(	attributs => hash contenant tout les arguments passes a la fonction convertPairToWig pour le header,
				strand => String : strand des données );
Parameters : see Usage
Returns :	$track_desc => String : header du fichier contenant les informations passees en arguments.
Version : v1.0

=end function

=cut
sub setTrackDescription{
	my %args = @_;
	if(ref($args{attributs}) ne 'HASH'){
		die "sub setTrackDescription: parameter attributs is not a valid hash_table.\n";
	}
	my %attr = %{$args{attributs}};
	my $strand;

	# definit les donnees en fonction de FWD ou REV pour l'analyse suivante.
	if($args{strand} eq '' || (uc($args{strand}) =~ m/FWD/) || (uc($args{strand}) =~ m/FORWARD/)){
		$strand = 'fwd';
	}else{
		$strand = 'rev';
	}

	# si aucun type n'est donne, on impose un type par default.
	if($attr{type} eq ""){
		$attr{type} = "wiggle_0";
	}
	# si l'autoScale est definit sur "off", on test le format de la viewLimits fixe.
	if($attr{autoScale} eq "off"){
		if(!($attr{viewLimits} =~ m/-*\d+:-*\d+/)){
			die "sub setTrackDescription: viewLimits is not in the standard format (lower:upper),please check it.";
		}
	}
	# on test si visibility possede l'un des trois bons arguments.
	if(!(uc($attr{visibility})) =~ m/full/ && !(uc($attr{visibility})) =~ m/dense/ && !(uc($attr{visibility})) =~ m/hide/){
		die "sub setTrackDescription: visibility is not in the standard format (full or dense or hide),please check it.";
	}
	# on test si il y a un attribut dans yLineMark et on verifie son format si c'est le cas.
	if($attr{yLineMark} && !($attr{yLineMark} =~ m/\d+/)){
		die "sub setTrackDescription: yLIneMark is not in the standard format (Number),please check it.";
	}
	# on test si il y a un attribut dans priority et on verifie son format si c'est le cas.
	if($attr{priority} && !($attr{priority} =~ m/\d+/)){
		die "sub setTrackDescription: priority is not in the standard format (Number),please check it.";
	}

	# on va creer le header avec les arguments de base que doit comporter une fiche wig.
	my $track_desc;
	# on creer d'abords pour les données FWD.
	if($strand eq 'fwd'){
		# on test si il y a un nom ou une description FWD en entree, sinon on en donne une par default.
		if($attr{labelFWD} eq ""){
			$attr{labelFWD} = "FWD";
		}
		# on test si il n'y a pas de code couleur FWD et on en donne une par defaults.
		if($attr{colorFWD} eq ""){
			$attr{colorFWD} = "0,0,153";
		# sinon si il y a une couleur FWD, on test le format de celle-ci.
		}elsif(!($attr{colorFWD} =~ m/\d+,\d+,\d+/)){
			die "sub setTrackDescription: colorFWD is not in the standard format (RRR,GGG,BBB),please check it."
		}
		$track_desc .= "track type=".$attr{type}." name=\"".$attr{labelFWD}."\" description=\"".$attr{labelFWD}."\" color=".$attr{colorFWD}." ";
	# creation du header pour les données REV.
	}else{
		# on test si il y a un nom ou une description REV en entree, sinon on en donne une par default.
		if($attr{labelREV} eq ""){
			$attr{labelREV} = "REV";
		}
		# on test si il n'y a pas de code couleur REV et on en donne une par defaults.
		if($attr{colorREV} eq ""){
			$attr{colorREV} = "255,0,0";
		# sinon si il y a une couleur REV, on test le format de celle-ci.
		}elsif(!($attr{colorREV} =~ m/\d+,\d+,\d+/)){
			die "sub setTrackDescription: colorREV is not in the standard format (RRR,GGG,BBB),please check it."
		}
		$track_desc .= "track type=".$attr{type}." name=\"".$attr{labelREV}."\" description=\"".$attr{labelREV}."\" color=".$attr{colorREV}." ";
	}
	# on va creer les arguments definissant la track FWD en fonction des parametres passes ou avec les valeurs par defaut.
	# si l'arguments "visibility" n'est pas vide, on ajoute cette arguments au header.
	if($attr{visibility}){
		$track_desc .= "visibility=".$attr{visibility}." ";
	}
	# si l'arguments "autoScale" contient "off", le header comportera l'autoScale ainsi que la viewLimits fixé.
	if($attr{autoScale} eq "off"){
		$track_desc .= "autoScale=".$attr{autoScale}." viewLimits=".$attr{viewLimits}." ";
	# si l'arguments "autoScale" est sur "on", le header ne comportera que l'autoScale avec la valeur "on".
	}else{
		$track_desc .= "autoScale=".$attr{autoScale}." ";
	}
	# si l'arguments "yLineMark" n'est pas vide, on ajoute cette arguments au header.
	if($attr{yLineMark}){
		$track_desc .= "yLineMark=".$attr{yLineMark}." ";
	}
	# si l'arguments "yLineOnOff" n'est pas vide, on ajoute cette arguments au header.
	if($attr{yLineOnOff} eq "on"){
		$track_desc .= "yLineOnOff=".$attr{yLineOnOff}." ";
	}
	# si l'arguments "priority" n'est pas vide, on ajoute cette arguments au header.
	if($attr{priority}){
		$track_desc .= "priority=".$attr{priority}." ";
	}

	return $track_desc;
}

=begin function

Function : printWIG
Description :	cette fonction permet de printer les valeurs dans un fichier en fonction de FWD ou REV et avec le bon header correspondant.
Usage : printWIG(	datas => ,
			probes_length => string : taille des probes,
			track_desc => String : header du fichier cree avant avec la fonction setTrackDescription,
			file_out => string : path du fichier Wig genere en sorti);
Parameters :	 datas => $VAR1 = {
					'chr3' => [
							'Value',
						]
				}
Returns : none
Version : v1.0

=end function

=cut
sub printWIG{
	my %args = @_;
	my %datas = %{$args{datas}};
	my $probes_length = $args{probes_length};
	my $track_desc = $args{track_desc};
	my $file_out = $args{file_out};

	open (OUT,">$file_out") or die "sub printWIG: Unable to create the file $file_out .\n";

	foreach my $k (keys(%datas)){

		# on ecrit la ligne d'entete. 
		print OUT $track_desc."\n";
		print OUT "variableStep chrom=".$k."\n";
		for (my $i = 1; $i <= scalar(@{$datas{$k}}); $i++){
			next if(!$datas{$k}[$i]);
			# premiere position d'une probe.
			# on recupere sa valeur.
			my $cur_val = $datas{$k}[$i];
			# pour toutes les positions de la probe.
			for (my $j = 1 ; $j <= $probes_length; $j++){
				if($j == 1){
					# print "position ($i) valeur $cur_val.
					print OUT $i."\t".$cur_val."\n";
				# si on arrive sur une nouvelle probe.
				}elsif(@{$datas{$k}}[$i]){
					$cur_val = $datas{$k}[$i];
					# print "position ($i) valeur $cur_val.
					print OUT $i."\t".$cur_val."\n";
					$j=1;
				}else{
					# print "position ($i) valeur $cur_val.
					print OUT $i."\t".$cur_val."\n";
				}
				$i++;
			}
			$i--;
		}
	}
	close OUT;
}

=begin function

Function : convertPairToWig
Description :	fonction qui va convertir le fichier pair passer en argument, en fichier wig_fwd et wig_rev.
		si un hash de valeurs est passé en argumant, alors ces valeurs seront associées.
		associe chaque valeur a une probe, si on rencontre une nouvelle probe, alors on prend la valeur de cette nouvelle probe.
		traite les valeurs des probes en forward et reverse.
		plusieurs options sont à définir, elles permettent de fixer :
 			le centre automatiquement(autoScale->on/off) ou manuellement(viewLimits->min:max),
 			la couleur de la courbe resultat(color->RRR,GGG,BBB),
			la visibilité de la courbe(visibility->full/dense/hide),
 			la valeur min de y pour défnir une valeur seuil(yLineMark->on/off),
			la visibilité de la ligne y(yLineOnOff->on/off),
			la prorité(priority->N)	
		si un seuil est fourni en parametre, alors cette fonction peut directement genere un fichier wig avec les probes seulement exprime.
Usage : convertPairToWig(	pair_file => String : tableau des path des fichier pair,
				wig_file_fwd => String : path du fichier WIG_fwd genere en sortie,
				wig_file_rev => String : path du fichier WIG_rev genere en sortie,
				vals => ,
				type => ,
				id_type =>,
				labelFWD => ,
				labelREV => ,
				probes_length => ,
				autoScale => ,
				viewLimits => ,
				visibility => ,
				yLineMark => ,
				yLineOnOff => ,
				colorFWD => ,
				colorREV => ,
				priority => ,
				filter_value_min => string : valeur seuil calculee dans avec la fonction);
Parameters :	vals => $VAR1 = {
					'ProbeID' => 'Value',
				}
Returns : none
Version : v1.0

=end function

=cut	
sub convertPairToWig{
	my %args = @_;
	my $pair_file = $args{pair_file};
	my $vals = $args{vals};
	my %conf = %{$args{conf}};
	my $id_type = $args{id_type};
	my $use_strand = $args{use_strand};
	my $probes_length = $args{probes_length};
	my $nb_controls;
	my $nb_probes_fwd;
	my $nb_probes_rev;
	my $nb_probes;
	my $import_values;
	my $good_cats = $conf{good_cats};
	if(ref($vals) eq 'HASH'){
		$import_values = 1;
	}
	if($probes_length == 0){
			$probes_length = 60;
	}
	
	open (IN,$pair_file) or die "sub convertPairToWig: Unable to open the file $pair_file .\n";

        my %chrs_fwd;
        my %chrs_rev;

	while (<IN>){
		chomp();
		my @line = split(/\t/);
		# identification de la sequence.
		$line[$conf{seq_col}-1] =~ m/^([^\s]*):\d+-.+$/;
		my $seq = $1;
		# si pas de sequence identifiee on est en random!.
		if(!$seq){
			$nb_controls++;
			next;
		}
		my %view;
		next if !$$good_cats{@line[$conf{categorie_col}-1]};
		my $value;
		# make the probe_id 
		my $strand = getProbeStrandFromPair(	conf => $conf,
							line => \@line);
		my $probe_id = getProbeIDFromPair(	conf => $conf,
							strand => $strand,
							line => \@line,
							use_strand => $use_strand,
							id_type => $id_type);
		if($import_values){
			$value = $$vals{$probe_id};
		}else{
			$value = $line[$conf{value_col}-1];
		}
		# si on a deja vu cet id, alors on pass
		next if ($view{$probe_id});
		$view{$probe_id} = 1;
		
		# si on a definit des seuils de filtrage max ou min et que les valeurs les depassent, on ne les prend pas.
		if(($args{filter_value_min} && ($value <= $args{filter_value_min})) || ($args{filter_value_max} && ($value >= $args{filter_value_max}))){
			next;
		}else{
			# si on est en forward.
			if($strand eq 'FWD' || !$use_strand){
				# si on deja vu cette sequence.
				if(ref($chrs_fwd{$seq}) eq "ARRAY"){
					$chrs_fwd{$seq}[$line[$conf{position_col}-1]] = $value;
				}else{
					my @temp2;
					$temp2[$line[$conf{position_col}-1]] = $value;
					$chrs_fwd{$seq} = \@temp2;
				}
			# si on est en reverse.
			}else{
				# si on a deja vu cette sequence.
				if(ref($chrs_rev{$seq}) eq "ARRAY"){
					$chrs_rev{$seq}[$line[$conf{position_col}-1]] = $value;
				}else{
					my @temp2;
					$temp2[$line[$conf{position_col}-1]] = $value;
					$chrs_rev{$seq} = \@temp2;
				}
			}
		}
	}
	
	# appel de la fonction permettant la construction du header en fonction de FWD ou REV.
	my $track_FWD_args = setTrackDescription(	attributs => \%args,
							strand => 'fwd' );
	# on print dans le fichier le bon header et les donnees en fonction de FWD ou REV.
	printWIG(	datas => \%chrs_fwd,
			probes_length => $probes_length,
			track_desc => $track_FWD_args,
			file_out => $args{wig_file_fwd});

	if($use_strand){
		my $track_REV_args = setTrackDescription(	attributs => \%args,
								strand => 'rev' );

		printWIG(	datas => \%chrs_rev,
				probes_length => $probes_length,
				track_desc => $track_REV_args,
				file_out => $args{wig_file_rev});
	}
	close IN;
}

=begin function

Function : getValues
Description : fonction qui va lire le fichier d'entree et renvoyer un hash contenant les associassions ID-value.
Usage : %{getValues(	file => string : path du fichier des valeurs normalisees et les moyennes,
			value_column => string : nombre de fichier +2 pour permettre de prendre les bonne valeurs dans les colonnes,
			id_column => 1,
			header => 1 )}
Parameters :  see Usage
Returns : 	\%values => $VAR1 = {
					'ProbeID' => 'Value',
				}
Version : v1.0

=end function

=cut
sub getValues {
	my %args = @_;
	my $file = $args{file};
	my $value_column = $args{value_column};
	my $id_column = $args{id_column};
	my $header = $args{header};

	my %values;
	
	open (IN, $file) or die "sub getValues: Unable to open the file $file .\n";
	my $ct = -1;
	while (<IN>) {
		$ct++;
		next if($ct<$header);
		chomp();
		my @line = split(/\s/);
		$values{$line[$id_column-1]} = $line[$value_column-1];
	}
	close IN;
	return \%values;
}

=begin function

Function : calcMean
Description : 	cette fonction va calculer la moyenne par ligne du fichier passer en argumant.
		si l'option 'add"' est spécifiée, alors les valeurs des moyennes seront ajoutées
		dans une nouvelle colonne à la fin du fichier, dans le cas contraire le fichier créé ne contiendra que la colonne des moyennes.
Usage : calcMean($normalized_file,$norm_mean_file,'add',$temp_dir,scalar(@files));
Parameters :	$normalized_file => string : path du fichier contenant les valeurs normalisees.
		$norm_mean_file => string : path du fichier de sortie.
		'add' => string : option.
		$temp_dir => string : path du fichier contenant les fichier pair.
		scalar(@files) => string : nombre de fichier contenu dans le repertoire $temp_dir.
Returns :	$file_out => String : path du fichier de sortie contenant les valeurs normalisé et les moyennes.
Version : v1.0

=end function

=cut
sub calcMean {
	my %args = @_;
	my $file_in = $args{file_in};
	my $file_out = $args{file_out};
	my $temp_dir = $args{temp_dir};

	# ecriture du script R.
	my $file = $temp_dir."/R_mean_".time.".temp.r";
	open (R_mean, ">".$file);
	
	print " |-create R script\n";
	
	print R_mean 'setwd("'.$temp_dir.'")'."\n";
	print R_mean "data_in = read.table(\"$file_in\",header=TRUE)\n";
	print R_mean "data_temp = data_in[,2:length(data_in)]\n";
	print R_mean "mean_values = rowMeans(data_temp)\n";
	print R_mean "col_names = names(data_in)\n";
	print R_mean "data_in[,length(data_in)+1] = mean_values\n";
	print R_mean "col_names[length(col_names)+1] = \"Mean\"\n";
	print R_mean "names(data_in)=col_names\n";
	print R_mean "\n";

	print R_mean "# ecriture du fichier de resultat.\n";
	print R_mean "write.table(data_in,file=\"$file_out\",row.names = FALSE, quote = FALSE)\n\n" ;
	close R_mean;

	print " |-run R script\n";
	
	# lancement du script R.
	runR->lunchRScript($file);
	print " |-end of mean step\n";
# 	my $del_cmd = "rm -rf $file";
# 	`$del_cmd`;

	return $file_out;
}

=begin function

Function : normalizeDatas
Description : 	fonction qui va normaliser les valeurs contenues dans le fichier passé en argumant.
		cette normalisation centree sera efectuée sur chaque colonne.
Usage : normalizeDatas($raw_file,$normalized_file,$temp_dir,scalar(@files));
Parameters :	 $raw_file => string : path du fichier.
		$normalized_file => string : path du fichier contenant les valeurs normalisees.
		$temp_dir => string : path du fichier contenant les fichier pair.
		scalar(@files) => string : nombre de fichier contenu dans le repertoire $temp_dir.
Returns :	$file_out => string :  path du fichier de sorti contenant les resultats.
Version : v1.0

=end function

=cut
sub normalizeDatas {
	my $file_in = shift;
	my $file_out= shift;
	my $temp_dir = shift;
	my $nb_sample = shift; 

	# ecriture du script R.
	my $file = $temp_dir."/R_norm_".time.".temp.r";
	open (R_norm, ">".$file);
	
	print R_norm 'setwd("'.$temp_dir.'")'."\n";
	print R_norm "data_in = read.table(\"$file_in\",header=TRUE)\n";
	print R_norm "data_out = data_in\n";
	print R_norm "# calcul des valeurs normalisees\n"; 
	print R_norm "for (i in 2:".($nb_sample+1).") {\n";
	print R_norm "\ttemp = data_in[,i]\n";
	print R_norm "\tlog2vals = log2(temp)\n";
	print R_norm "\tvals_norm = scale(log2vals,center=TRUE,scale=FALSE) \n";
	print R_norm "\tdata_out[,i] = vals_norm\n";
	print R_norm "}\n";

	# calcul de la moyenne de ces valeurs.
	print " |-create R script\n";
	print R_norm "# ecriture du fichier de resultat.\n";
	print R_norm "write.table(data_out,file=\"$file_out\",row.names = FALSE, quote = FALSE)\n\n" ;
	close R_norm;

	print " |-run R script\n";
	# lancement du script R.
	runR->lunchRScript($file);
	print " |-end of normalisation step\n";
# 	my $del_cmd = "rm -rf $file";
# 	`$del_cmd`;

	return $file_out;
}

=begin function

Function : calcThreshold
Description :	cette fonction genere un script R calculant la valeur seuil pour laquelle un gene sera considere comme exprimer ou non.
		cette fonction peut egalement genere un fichier pdf contenu un graphe avec les differente courbe et la valeur seuil.
Usage : calcThreshold(	file_in => String : path du fichier GFF d'entree,
			plot_file => String : path du fichier de sorti .pdf,
			temp_dir => String : Path du repertoire contenant les fichiers pair,
			value_column => String : valeur de la column contenant les informations voulu,
			header => String : TRUE or FALSE pour savoir si il y a un header ou non,
			FDRthresh => String : );
Parameters : see Usage
Returns :	$trh_val => String : Valeur du seuil calcule.
Version : v1.0

=end function

=cut
sub calcThreshold {
	my %args = @_;
	my $file_in = $args{file_in};
	my $plot_file = $args{plot_file};
	my $temp_dir = $args{temp_dir};
	my $value_column = $args{value_column};
	my $FDRthresh = $args{FDRthresh};
	my $header = $args{header};

	# on test si le parametre header est renseigne car c'est un parametre obligatoire.
	if(!$header){
		die "sub calcThreshold: header parameter is missing, please check it.";
	# si l'argument header est rensigne, alors on test son format.
 	}elsif($header ne 'TRUE' && $header ne 'FALSE'){
		die "sub calcThreshold: header is not in the standard format (TRUE or FALSE), please check it.";
	}

	# on test si FDRthresh existe sinon on lui donne une valeur par default.
	if(!$FDRthresh){
		$FDRthresh = '1e-3';
	# si FDRthresh existe on test son format.
	}elsif(!($FDRthresh =~ m/0\.d+/) && !($FDRthresh =~ m/[1-9]e\-[1-9]+/)){
		die "sub calcThreshold: FDRthresh is not in the standard format (ex: 0.001 or 1e-3), please check it.";
	}

	# ecriture du script R.
	my $file = $temp_dir."/R_threshold_".time.".temp.r";
	my $thresh_value_file = $temp_dir."/R_threshold_value_".time.".temp.r";
	open (R_threshold, ">".$file);
	
	print R_threshold "# On charge les differentes library necessaire.\n";
	print R_threshold "library(\"multtest\")\n";
	print R_threshold "library(\"genefilter\")\n";

	print R_threshold "# on definit le repertoire courant.\n";
	print R_threshold 'setwd("'.$temp_dir.'")'."\n";

	print R_threshold "# retrieve all dats from gff file.\n";
	print R_threshold "norm_mean_values = read.table (\"$file_in\", header=$header,sep=\"\\t\")\n";

	print R_threshold "# extract expression values from data_frame.\n";
	print R_threshold "levu = norm_mean_values[,$value_column]\n";

	print R_threshold "# loc va determiner la valeur autour de laquelle sera centrée la courbe gaussienne d'expression théorique qui sera générée.\n";
	print R_threshold "loc = shorth(levu, na.rm=TRUE)\n";
	print R_threshold "#shorth: The shorth is the shortest interval that covers half of the values in x.\n";
	print R_threshold "	#This function calculates the mean of the x values that lie in the shorth.\n";
	print R_threshold "	#This was proposed by Andrews (1972) as a robust estimator of location.\n";

	print R_threshold "# z est le vecteur qui contient toules les valeurs de la partie gauche de la gaussienne théorique.\n";
	print R_threshold "z = levu [which(levu<=loc)]-loc\n";

	print R_threshold "# on creer les valeurs symetriques de z pour générer la partie droite de la gaussienne.\n";
	print R_threshold "scale = mad(c(z,-z))\n";
	print R_threshold "#****# scale_control=0.2457526 (MAD)(2009-11-26)\n";

	print R_threshold "pn = pnorm(q=levu, mean=loc, sd=scale, lower.tail=FALSE)\n";
	print R_threshold "bh = mt.rawp2adjp(pn, proc=\"BY\")\n";
	print R_threshold "adjp = numeric(nrow(bh\$adjp))\n";
	print R_threshold "adjp[bh\$index] = bh\$adjp[,2]\n";
	print R_threshold "selfdr = (adjp < $FDRthresh)\n";
	print R_threshold "data_thresh = cbind(levu,selfdr)\n";
	print R_threshold "Tem1 = cbind(selfdr,levu)\n";
	print R_threshold "Temorder = which(Tem1[,1]==1)\n";
	print R_threshold "Tem2 = Tem1 [Temorder,]\n";
	print R_threshold "data_thresh_FDR_1e_3 = min (Tem2)\n";
	print R_threshold "thresh= min (Tem2)\n";
	print R_threshold "d2 = density(levu, na.rm=TRUE)\n";
	print R_threshold "write.table(file=\"$thresh_value_file\",thresh,row.names = FALSE,quote = FALSE)\n";

	print R_threshold "# Si on veut genere un pdf on met le graphique genere dans un fichier de sortie au format pdf.\n";
	if($plot_file){
		print R_threshold "pdf(\"$plot_file\")\n";
		print R_threshold "plot(d2, col=\"red\", ann = FALSE, lwd=1)\n";
		print R_threshold "title(main=\"Expression Threshold\", xlab=\"Probes Values\", ylab=\"Density values\")\n";
		print R_threshold "legend(\"topright\",title = \"legend\", c(\"density\", \"norm\", loc, thresh), cex=0.8, col=c(\"red\", \"orange\", \"black\", \"blue\"), lwd=2, bty=\"o\")\n";
		print R_threshold "zz = c(z,-z)\n";
		print R_threshold "dzz = density(zz)\n";
		print R_threshold "dn = dnorm (x=dzz\$x, mean=loc, sd=scale)\n";
		print R_threshold "lines(dzz\$x, dn/max(dn)*max(d2\$y), col=\"orange\")\n";
		print R_threshold "abline(v=c(loc, thresh), col=c(\"black\", \"blue\"))\n";
		print R_threshold "dev.off()\n"; 
	}

	print " |-create R script\n";

	close R_threshold;

	print " |-run R script\n";
	# lancement du script R.
	runR->lunchRScript($file);
	print " |-end of calcThreshold step\n";
# 	my $del_cmd = "rm -rf $file";
# 	 `$del_cmd`;

	# on recupere la valeur seuil pour ensuite pouvoir la renvoyer.	
	open (IN,$thresh_value_file) or die "sub calcThreshold: Unable to open the file $thresh_value_file .\n";
	# on saute la premiere ligne qui contient "x".
	<IN>;
	# on recupere la valeur de la deuxieme ligne qui est la valeur seuil.
	my $trh_val = <IN>;
	close IN;
	# on supprime le fichier temporaire genere pour recuperer la valeur seuil.
	my $del_cmd = "rm -rf $thresh_value_file";
#	 `$del_cmd`;

	# retourne la valeur calculee du seuil.
	return $trh_val;


}

=begin function

Function : createDatasetFile
Description : fonction qui va créer le fichier tabuler contenant l'ensemble des valeurs des differents fichiers pair trouvés dans les repertoire.
Usage : createDatasetFile(\%values,\@ids,$temp_dir,\@files);
Parameters :	\%values => $VAR1 = { 
					'String : ProbeID' [ 
								String : value pair file
							]
				}
		\@sample => $VAR1 = [ 
					'String : ProbeID' 
				]
		$in_dir => String : Path du repertoire contenant les fichiers pair
		\@files => $VAR1 = [
					'String : fichier pair'
				];
Returns : $file => String : Path du fichier de sortie contenant les données.
Version : v1.0

=end function

=cut
sub createDatasetFile {
	(my $values, my $ids, my $dir, my $files) = @_;
	my $file = $dir."/".time.".temp";
	open (DATASET,">".$file) or die "sub createDatasetFile: unable to create a temporary file in the $dir directory.";
	# creation des headers.
	print DATASET "PROBE_ID";
	foreach my $f (@{$files}){
		print DATASET "\t$f";
	}
	print DATASET "\n";

	# ecriture des valeurs.
	foreach my $id (@{$ids}){
		if(ref($$values{$id}) ne 'ARRAY'){
			die "sub createDatasetFile: no value for the probe $id.\n";
		}
		if(scalar(@{$$values{$id}}) != scalar(@{$files})){
			die "sub createDatasetFile: the probe $id get  ".scalar(@{$$values{$id}})." values but the number of files is ".scalar(@{$files})."\n";
		}
		print DATASET $id;
		foreach my $val (@{$$values{$id}}){
			print DATASET "\t".$val;
		}
		print DATASET "\n";
	}
	close DATASET;
	return $file;
}

=begin function

Function : addSampleToGroupe
Description :	ajoute le sample au hash global qui contient l'ensemble de valeurs de chaque experience pour chaque probe.
		cette fonction renvoie la liste des identifiants des probes dans l'ordre ou ils apparaissent.
Usage : @{addSampleToGroupe(\%values,\@sample)}
Parameters : 	\%values => $VAR1 = { 
					'String : ProbeID' [ 
								String : value pair file
							]
				}
		\@sample => $VAR1 = [ 
					'String : ProbeID' 
				]
Returns : none
Version : v1.0

=end function

=cut
sub addSampleToGroupe{
	(my $values, my $sample) = @_;

	if(scalar(@{$sample}) == 0){
		die "addSampleToGroupe: empty sample values.\n";
	}
	my @id_list;
	foreach my $val (@{$sample}){
		my @temp = @{$val};
		push(@id_list,$temp[0]);
		if(ref($$values{$temp[0]}) eq "ARRAY"){
			push(@{$$values{$temp[0]}},$temp[1]);
		}else{
			my @temp2;
			push(@temp2,$temp[1]);
			$$values{$temp[0]} = \@temp2;
		}
	}
	return \@id_list;
}

=begin function

Function : getValuesFromPairFile
Description : renvoie les valeurs des hybridations contenues dans un fichier pair.
Usage :	@{getValuesFromPairFile($in_dir,$f)}
Parameters :	$rep => répertoire contenant le fichiers pair,
		$f => path of pair file.
		$id_type => 'loc' | 'id' type d'indentification souhaité par location ou par id
		$use_strand => 0 | 1 distinction entre les strnd ou non
		$config => reference of a hash which must contains the folowing keys 
		(probe_id_col => int,
		value_col => int,
		categorie_col => int,
		good_cats => ( cat1 => 1, cat2 => 1, cat3 => 1))

Returns : \@value => $VAR1 = 	[
					'String : ProbesID',
					'String : Value'
				];
Version : v1.0

=end function

=cut
sub getValuesFromPairFile {
	(my $rep, my $file, my $id_type, my $use_strand, my $config) = @_;

	my @value;
	my $good_cats = $$config{good_cats};
	my %vals;

	chdir($rep);
	opendir R, $rep or die "sub getValuesFromPairFile: impossible d'ouvrir le rép. $rep.";
	open PAIR, $file or die "sub getValuesFromPairFile: impossible d'ouvrir le fichier. $file.";
	# remove headers
	for (my $i = 0; $i <  $$config{nb_headers}; $i++){
		<PAIR>;
	}
	while (<PAIR>){
		my @ln = split(/\t/,$_);
		next if !$$good_cats{@ln[$$config{categorie_col}-1]};
		# make the probe_id 
		my $strand = getProbeStrandFromPair(	conf => $config,
							line => \@ln);
		my $probe_id = getProbeIDFromPair(	conf => $config,
							strand => $strand,
							line => \@ln,
							use_strand => $use_strand,
							id_type => $id_type);
		if (@ln[($$config{value_col}-1)] eq ""){
			die "Error :no value in the value_col column (".($$config{value_col}-1).")\n$_";
		}
		# we test if the $probe_id has been already seen
		if($vals{$probe_id}){
			# if we are in the 'id' mode two probe can't share the same ID
			if($id_type ne 'loc'){
				die "Error: the probe_id $probe_id is not unique !\n";
			# in 'loc' mode we take all the values corresponding to one probe
			}else{
				push(@{$vals{$probe_id}},@ln[($$config{value_col}-1)]);
			}
			
		}else{
			my @temp = (@ln[($$config{value_col}-1)]);
			$vals{$probe_id} = \@temp;
		}
	}
	# we merge the values if necessary
	foreach my $k (keys(%vals)){
		my $sum;
		my $nb;
		foreach my $val(@{$vals{$k}}){
			$sum = $sum+$val;
			$nb++;
		}
		my @final = ($k,($sum/$nb));
		push(@value,\@final);
	}
	return \@value;
	close PAIR;
}

=begin function

Function : getFileList
Description : renvoie la liste des fichiers contenus dans un repertoire.
Usage : @{getFileList($in_dir)}
Parameters :	 $in_dir => répertoire contenant les fichiers pair, $f => tous les fichiers pair.
Returns :	\@files => $VAR1 =	[
						String : path du fichier pair.
					];
Version : v1.0

=end function

=cut
sub getFileList {
	my $rep = shift;
	
	chdir($rep);
	opendir R, $rep or die "sub getFileList: impossible d'ouvrir le rép. $rep.";
	my @liste=readdir(R);
	my @files;
	foreach my $f (@liste) {
		 next if $f eq "." or $f eq "..";
		 if (-f $f) {
# 		  print "  |--".$f." "x(30-length($f))."-";
# 		  print "r" if (-r $f);
# 		  print "w" if (-w $f);
# 		  print "x" if (-x $f);
# 		  print "\t";
# 		  print -s $f;
# 		  print "\n";
			push(@files,$f);
		 }else{
# 		  print " d $f\n";
		 }
	}
	closedir R;
	return \@files;
}

=begin function

Function : par_pos_asc
Description : tri ascendant du tableau N*2 par rapport au contenu de la premiere colonne.
Usage : par_pos_asc @{};
Parameters : see Usage
Returns : none
Version : v1.0

=end function

=cut
sub par_pos_asc {
   my @ap = @{$a};
   my @bp = @{$b};
   if($ap[1] < $bp[1]){
      return -1;
   }	elsif($ap[1] == $bp[1]){
      return 0;
   }	elsif($ap[1] > $bp[1]){
      return 1;
   }
}

sub getProbeIDFromPair {
	my %args = @_;
	my $config = $args{conf};
	my @ln = @{$args{line}};
	my $use_strand = $args{use_strand};
	my $id_type = $args{id_type};
	my $strand = $args{strand};
	
	my $probe_id;
	if($id_type eq 'loc'){
		if ((@ln[($$config{seq_col}-1)] eq "") || (@ln[($$config{position_col}-1)] eq "")){
			die "Error :no sequence in the seq_col column (".($$config{seq_col}).") or no position in the position_col column (".($$config{position_col})."\n$_";
		}
		if($use_strand){
			$probe_id =  @ln[($$config{seq_col}-1)].$strand."-".@ln[($$config{position_col}-1)];
		}else{
			$probe_id = @ln[($$config{seq_col}-1)]."-".@ln[($$config{position_col}-1)];
		}
	}else{
		if (@ln[($$config{probe_id_col}-1)] eq ""){
			die "Error :no probe_id in the probe id column (".($$config{probe_id_col}).")\n$_";
		}
		$probe_id = @ln[($$config{probe_id_col}-1)];
	}
	return uc($probe_id);
}

sub getProbeStrandFromPair {
	my %args = @_;
	my $config = $args{conf};
	my @line = @{$args{line}};

	my $strand;
	if(uc($line[$$config{strand_col}-1]) =~ m/FORWARD/ || uc($line[$$config{strand_col}-1]) =~ m/FWD/){
		$strand = 'FWD';
	}else{
		$strand = 'REV';
	}
	return $strand;
}
