#!/usr/bin/perl

#------------------------------------------------#
#  Author:  Sylvain Marthey and Damien Barreau   #
#  Project: Tilseg                               #
#  Release: 1.0                                  #
#  Date :   17/12/2009                           #
#------------------------------------------------#

=head1 NAME

 TilSeg.pl

=head1 SYNOPSIS

 TilSeg.pl --gff_file <file path> --gff_out  <file path>--FDRthresh <Integer or String> --pdf_out <file path>

=head1 OPTIONS

	--gff_file		gff_file containing the values for thresold calculation

	--gff_out		gff_file containing the values tha are upper to the thresold

	--pdf_out		pdf file for values plotting

	--FDRthresh		

=over

=back

=head1 B<DESCRIPTION>


=cut

use strict;
use Getopt::Long;
use Pod::Usage;
use Data::Dumper;
use runR;

my $gff_file;
my $gff_out;
my $pdf_file;
my $FDRthresh;
my $help;

GetOptions(	"help|?" => \$help,
		"gff_file:s" => \$gff_file,
		"gff_out:s" => \$gff_out,
		"pdf_out:s" => \$pdf_file,
		"FDRthresh:s" => \$FDRthresh
	   )
  or
  pod2usage(-message=>"Try `$0' for more information.", -exitval => 2,-verbose=> 0);


if (($gff_file eq "") || !(-f($gff_file)))
  {
     pod2usage(-message=>"--gff_file parameter missing or is not  a valid file path.\nTry `$0 --help' for more information.",
	      -exitval => 2,-verbose=> 0);
  }
if (($pdf_file eq "" ) && !(-f($pdf_file)))
  {
     pod2usage(-message=>"--pdf_file parameter is not valid.\nTry `$0 --help' for more information.",
	      -exitval => 2,-verbose=> 0);
  }  
if(!$FDRthresh){
	$FDRthresh = '1e-3';
}elsif(!($FDRthresh =~ m/0\.d+/) && !($FDRthresh =~ m/[1-9]e\-[1-9]+/)){
     pod2usage(-message=>"--FDRthresh parameter is not in the standard format.\nTry `$0 --help' for more information.",
	      -exitval => 2,-verbose=> 0);
}


# we recover the threshold value.
################################################
print "calcul threshold\n"; ####################
################################################
my $threshold = calcThreshold(	file_in => $gff_file,
				plot_file => $pdf_file,
				temp_dir => "./",
				value_column => '6',
				header => 'TRUE',
				FDRthresh => $FDRthresh);

print "thresold value: $threshold\n";

if($gff_out){
	print "create the gff outpuut file\n";
	open (IN , $gff_file);
	open (OUT , ">".$gff_out) or die "$gff_out is not  a valid file path.\n";
	
	<IN>;
	print OUT $_;
	while(<IN>){
		my @ln = split(/\t/,$_);
		if(@ln[5]> $threshold){
			print OUT $_;
		}
	}
	
	close IN;
	close OUT;
}


=begin function

Function : calcThreshold
Description :	cette fonction genere un script R calculant la valeur seuil pour laquelle un gene sera considere comme exprimer ou non.
		cette fonction peut egalement genere un fichier pdf contenu un graphe avec les differente courbe et la valeur seuil.
Usage : calcThreshold(	file_in => String : path du fichier GFF d'entree,
			plot_file => String : path du fichier de sorti .pdf,
			temp_dir => String : Path du repertoire contenant les fichiers pair,
			value_column => String : valeur de la column contenant les informations voulu,
			header => String : TRUE or FALSE pour savoir si il y a un header ou non,
			FDRthresh => String : );
Parameters : see Usage
Returns :	$trh_val => String : Valeur du seuil calcule.
Version : v1.0

=end function

=cut
sub calcThreshold {
	my %args = @_;
	my $file_in = $args{file_in};
	my $plot_file = $args{plot_file};
	my $temp_dir = $args{temp_dir};
	my $value_column = $args{value_column};
	my $FDRthresh = $args{FDRthresh};
	my $header = $args{header};

	# on test si le parametre header est renseigne car c'est un parametre obligatoire.
	if(!$header){
		die "sub calcThreshold: header parameter is missing, please check it.";
	# si l'argument header est rensigne, alors on test son format.
 	}elsif($header ne 'TRUE' && $header ne 'FALSE'){
		die "sub calcThreshold: header is not in the standard format (TRUE or FALSE), please check it.";
	}

	# on test si FDRthresh existe sinon on lui donne une valeur par default.
	if(!$FDRthresh){
		$FDRthresh = '1e-3';
	# si FDRthresh existe on test son format.
	}elsif(!($FDRthresh =~ m/0\.d+/) && !($FDRthresh =~ m/[1-9]e\-[1-9]+/)){
		die "sub calcThreshold: FDRthresh is not in the standard format (ex: 0.001 or 1e-3), please check it.";
	}

	# ecriture du script R.
	my $file = $temp_dir."/R_threshold_".time.".temp.r";
	my $thresh_value_file = $temp_dir."/R_threshold_value_".time.".temp.r";
	open (R_threshold, ">".$file);
	
	print R_threshold "# On charge les differentes library necessaire.\n";
	print R_threshold "library(\"multtest\")\n";
	print R_threshold "library(\"genefilter\")\n";

	print R_threshold "# on definit le repertoire courant.\n";
	print R_threshold 'setwd("'.$temp_dir.'")'."\n";

	print R_threshold "# retrieve all dats from gff file.\n";
	print R_threshold "norm_mean_values = read.table (\"$file_in\", header=$header,sep=\"\\t\")\n";

	print R_threshold "# extract expression values from data_frame.\n";
	print R_threshold "levu = norm_mean_values[,$value_column]\n";

	print R_threshold "# loc va determiner la valeur autour de laquelle sera centrée la courbe gaussienne d'expression théorique qui sera générée.\n";
	print R_threshold "loc = shorth(levu, na.rm=TRUE)\n";
	print R_threshold "#shorth: The shorth is the shortest interval that covers half of the values in x.\n";
	print R_threshold "	#This function calculates the mean of the x values that lie in the shorth.\n";
	print R_threshold "	#This was proposed by Andrews (1972) as a robust estimator of location.\n";

	print R_threshold "# z est le vecteur qui contient toules les valeurs de la partie gauche de la gaussienne théorique.\n";
	print R_threshold "z = levu [which(levu<=loc)]-loc\n";

	print R_threshold "# on creer les valeurs symetriques de z pour générer la partie droite de la gaussienne.\n";
	print R_threshold "scale = mad(c(z,-z))\n";
	print R_threshold "#****# scale_control=0.2457526 (MAD)(2009-11-26)\n";

	print R_threshold "pn = pnorm(q=levu, mean=loc, sd=scale, lower.tail=FALSE)\n";
	print R_threshold "bh = mt.rawp2adjp(pn, proc=\"BY\")\n";
	print R_threshold "adjp = numeric(nrow(bh\$adjp))\n";
	print R_threshold "adjp[bh\$index] = bh\$adjp[,2]\n";
	print R_threshold "selfdr = (adjp < $FDRthresh)\n";
	print R_threshold "data_thresh = cbind(levu,selfdr)\n";
	print R_threshold "Tem1 = cbind(selfdr,levu)\n";
	print R_threshold "Temorder = which(Tem1[,1]==1)\n";
	print R_threshold "Tem2 = Tem1 [Temorder,]\n";
	print R_threshold "data_thresh_FDR_1e_3 = min (Tem2)\n";
	print R_threshold "thresh= min (Tem2)\n";
	print R_threshold "d2 = density(levu, na.rm=TRUE)\n";
	print R_threshold "write.table(file=\"$thresh_value_file\",thresh,row.names = FALSE,quote = FALSE)\n";

	print R_threshold "# Si on veut genere un pdf on met le graphique genere dans un fichier de sortie au format pdf.\n";
	if($plot_file){
		print R_threshold "pdf(\"$plot_file\")\n";
		print R_threshold "plot(d2, col=\"red\", ann = FALSE, lwd=1)\n";
		print R_threshold "title(main=\"Expression Threshold\", xlab=\"Probes Values\", ylab=\"Density values\")\n";
		print R_threshold "legend(\"topright\",title = \"legend\", c(\"density\", \"norm\", loc, thresh), cex=0.8, col=c(\"red\", \"orange\", \"black\", \"blue\"), lwd=2, bty=\"o\")\n";
		print R_threshold "zz = c(z,-z)\n";
		print R_threshold "dzz = density(zz)\n";
		print R_threshold "dn = dnorm (x=dzz\$x, mean=loc, sd=scale)\n";
		print R_threshold "lines(dzz\$x, dn/max(dn)*max(d2\$y), col=\"orange\")\n";
		print R_threshold "abline(v=c(loc, thresh), col=c(\"black\", \"blue\"))\n";
		print R_threshold "dev.off()\n"; 
	}

	print " |-create R script\n";

	close R_threshold;

	print " |-run R script\n";
	# lancement du script R.
	runR->lunchRScript($file);
	print " |-end of calcThreshold step\n";
	my $del_cmd = "rm -rf $file";
	 `$del_cmd`;

	# on recupere la valeur seuil pour ensuite pouvoir la renvoyer.	
	open (IN,$thresh_value_file) or die "sub calcThreshold: Unable to open the file $thresh_value_file .\n";
	# on saute la premiere ligne qui contient "x".
	<IN>;
	# on recupere la valeur de la deuxieme ligne qui est la valeur seuil.
	my $trh_val = <IN>;
	close IN;
	# on supprime le fichier temporaire genere pour recuperer la valeur seuil.
	my $del_cmd = "rm -rf $thresh_value_file";
	 `$del_cmd`;

	# retourne la valeur calculee du seuil.
	return $trh_val;


}

