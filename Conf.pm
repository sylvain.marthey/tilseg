#!/usr/bin/perl

package Conf;

##############################
# Marthey Sylvain GABI-GIS   #
# 17/12/2005                 #
# v1.0                       #
##############################

# This module contains all informations needed byt the TilSeg Software
use strict;

our $R_path = q{/usr/local/public/R/bin/R};

sub getFileConfig {

	my $conf_name = shift;

	if($conf_name eq 'Nimblegen'){
		my %conf;
		$conf{nb_headers} = 2;
		$conf{categorie_col} = 2;
		$conf{probe_id_col} = 4;
		$conf{value_col} = 10;
		$conf{seq_col} = 3;
		$conf{position_col} = 5;
		$conf{strand_col} = 2;
		my %goodCats = ("FORWARD" => 1,"REVERSE" => 1);
		$conf{good_cats} = \%goodCats;
		return \%conf;
	}elsif($conf_name eq 'default'){
		my %conf;
		$conf{nb_headers} = 2;
		$conf{categorie_col} = 2;
		$conf{probe_id_col} = 4;
		$conf{value_col} = 10;
		$conf{seq_col} = 3;
		$conf{position_col} = 5;
		$conf{strand_col} = 2;
		my %goodCats = ("FORWARD" => 1,"REVERSE" => 1);
		$conf{good_cats} = \%goodCats;
		return \%conf;
	}else{
		die "Error : the configuration you want to use is not define. Clease check the Conf.pm file"
	}

}

