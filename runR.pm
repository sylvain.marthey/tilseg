#!/usr/bin/perl

package runR;

##############################
# Marthey Sylvain GABI-GIS   #
# 17/12/2005                 #
# v1.0                       #
##############################

# This module all the methods nedded to lunch R script on the
# MIGALE Bioinformatics servors or cluster.
use strict;
use Conf;

sub lunchRScript {
	(my $self, my $filePath) = @_;
	if(!-f$filePath){
		die "Undefined R script path: $filePath\n";
	}elsif(!$Conf::R_path){
		die "Undefined R_path value in Config.pm";
	}else{
		my $cmd  = $Conf::R_path ." CMD BATCH ".$filePath."";
		# lunching of the R script
		my $sortie = `$cmd`;
		# removing the R output
		my @temp = split(/\//,$filePath);
		my $suff = pop(@temp);
		my $cmd = "rm -rf $suff.Rout";
		`$cmd`;
	}
	return;
}

return 1;